import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//pages============================================
import { MyApp } from './app.component';
import { IntroPage } from '../pages/intro/intro';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { MainPage } from '../pages/main/main';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { CategoriesPage } from '../pages/categories/categories';
import { ProductPage } from '../pages/product/product';
import { CheckoutSummaryPage } from '../pages/checkout_summary/checkout_summary';
import { CheckoutPage } from '../pages/checkout/checkout';
import { PaymentPage } from '../pages/payment/payment';
import { ModalAddressPage } from '../pages/modal-address/modal-address';
import { OrderDetailsPage } from '../pages/order-details/order-details';
import {AboutUsPage} from '../pages/about-us/about-us';
import {PrivacyPolicyPage} from '../pages/privacy-policy/privacy-policy';
import {FaqPage} from '../pages/faq/faq';
//pages============================================


import { PushkartServiceProvider } from '../providers/pushkart-service/pushkart-service';
import { HttpModule } from '@angular/http';
import { CartserviceProvider } from '../providers/cartservice/cartservice';
import { UserServicesProvider } from '../providers/user-services/user-services';
import { IonicStorageModule } from '@ionic/storage';
@NgModule({
  declarations: [
    MyApp,
    IntroPage,
    CategoriesPage,
    MainPage,
    LoginPage,
    SignupPage,
    CheckoutSummaryPage,
    ProductPage,
    CheckoutPage,
    PaymentPage,
    MyaccountPage,
    ModalAddressPage,
    OrderDetailsPage,
    AboutUsPage,
    PrivacyPolicyPage,
    FaqPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    /* IonicPageModule.forChild(CategoriesPage), */
    /* IonicPageModule.forChild(MainPage),
    IonicPageModule.forChild(CheckoutSummaryPage),
    IonicPageModule.forChild(ProductPage),
    IonicPageModule.forChild(LoginPage),
    IonicPageModule.forChild(IntroPage),
    IonicPageModule.forChild(CheckoutPage),
    IonicPageModule.forChild(PaymentPage), */
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage,
    IntroPage,
    CategoriesPage,
    LoginPage,
    SignupPage,
    CheckoutSummaryPage,
    CheckoutPage,
    ProductPage,
    PaymentPage,
    MyaccountPage,
    ModalAddressPage,
    OrderDetailsPage,
    AboutUsPage,
    PrivacyPolicyPage,
    FaqPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    PushkartServiceProvider,
    CartserviceProvider,
    UserServicesProvider
  ]
})
export class AppModule { }
