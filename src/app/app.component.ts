import { Component, ViewChild } from '@angular/core';
import { AppAvailability, } from '@ionic-native/app-availability';
import { Device } from '@ionic-native/device';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
//pages==============================================
import { IntroPage } from '../pages/intro/intro';
import { MainPage } from '../pages/main/main';
import { LoginPage } from '../pages/login/login';
import { CategoriesPage } from '../pages/categories/categories';
import { CheckoutSummaryPage } from '../pages/checkout_summary/checkout_summary';
import { PushkartServiceProvider } from '../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../providers/cartservice/cartservice';
import { UserServicesProvider } from '../providers/user-services/user-services';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { AboutUsPage } from '../pages/about-us/about-us';
import { TermsandconditionsPage } from '../pages/termsandconditions/termsandconditions';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { FaqPage } from '../pages/faq/faq';
//pages==============================================
@Component({
  templateUrl: 'app.html',
  providers: [
    PushkartServiceProvider,
    CartserviceProvider,
    UserServicesProvider,
    AppAvailability,
    Device,
    //Network
  ],
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'HomePage';
  public scheme: any;
  loadingMain: any;
  networkStatus: any;
  menuClass:string = 'close';
  url: string = 'https://v3.pushkart.ph/dev3/';

  pages: Array<{ title: string, component: any }>;
  categories: Array<any>;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private pushkartService: PushkartServiceProvider,
    private cartservice: CartserviceProvider,
    private menuCtrl: MenuController,
    private userService: UserServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public appAvailability: AppAvailability,
    public device: Device,
    public storage: Storage,
  ) {
    //this.loadingMain = loadingCtrl.create({ content: 'loading...' });
    //this.loadingMain.present();
    userService.setUrl(this.url);
    pushkartService.setUrl(this.url);
    cartservice.setUrl(this.url);
    this.initializeApp();
    this.openPageRoot();
    this.getCategories();
    cartservice.getPromo();
  }

  isNumeric(value) {
    return /^\d+$/.test(value);
  }

  getCategories() {
    console.log('getting files');
    this.categories = [];
    this.pushkartService.getCategories().subscribe(
      data => {

        for (var key in data) {
          data[key]['viewchild'] = false;
          data[key]['viewchild_more'] = false;
          data[key]['class'] = 'subcat-container';
        }
        this.categories = data;
        console.log('categories %o', data);
        this.pushkartService.category = data;
        //setTimeout(this.openPageRoot(),1000);
        //this.loadingMain.dismiss();
      },
      err => {
        //this.loadingMain.dismiss();
        console.log('error ' + err);
      }
    )
  }

  menuToggle(toggle) {
    if(toggle == 'open') {
      this.menuClass = 'open';
      console.log('menu'+toggle);
    }
    else {
      this.menuClass = 'close';
      console.log('menu'+toggle);
    }
  }

  openSubcat(index) {
    this.categories[index].viewchild_more = !this.categories[index].viewchild_more
    if (this.categories[index].viewchild) {
      this.categories[index].class = 'subcat-container accordionOut';
      setTimeout(() => {
        this.categories[index].class = 'subcat-container';
        this.categories[index].viewchild = !this.categories[index].viewchild;
      }, 450);
    } else {
      this.categories[index].viewchild = !this.categories[index].viewchild;
    }
  }


  isMaximum(value) {
    //console.log('value kart item: '+value);
    try {
      if (value.qty < this.cartservice.cart[value.product_id].stock_item) {
        return false;
      } else {
        return true;
      }
    } catch (err) {
      return false;
    }
  }

  openPageRoot() {
    this.rootPage = 'HomePage';
  }

  closeMyKart() {
    this.menuCtrl.close('mykart');
  }

  pushdata() {
    this.cartservice.cart = {};
    for (var key in this.pushkartService.product_list) {
      this.pushkartService.product_list[key].qty = 0;
      this.cartservice.cart[this.pushkartService.product_list[key].entity_id] = this.pushkartService.product_list[key];
    }
    console.log('cart %o', this.cartservice.cart);
  }

  clearkart() {
    if (this.cartservice.cart_count > 0) {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      let alert = this.alertCtrl.create({
        title: 'success',
        subTitle: 'cart items removed',
        buttons: ['dismiss']
      });
      setTimeout(() => {
        loading.dismiss();
        alert.present();
        this.closeMyKart();
      }, 3000);
      this.cartservice.clearkart();
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.push(CategoriesPage, { 'category': page });
  }

  goHome() {
    let view = this.nav.getActive();
    console.log(view.component.name);
    if (view.component.name == 'MainPage')
      console.log('Home');
    else
      this.nav.popToRoot();
  }

  getActivePage(): string {
    try {
      var component = this.nav.getActive().pageRef().nativeElement.tagName;
      return component;
    } catch (err) {
      return 'PAGE-MAIN';
    }
  }

  openTermsandConditions() {
    this.nav.push(TermsandconditionsPage);
  }

  openLogin() {
    this.nav.push(LoginPage);
  }

  openLogout() {
    let loading = this.loadingCtrl.create({
      content: 'logging out...'
    });
    loading.present();
    this.cartservice.clearLogout();
    setTimeout(() => {
      loading.dismiss();
      this.userService.logout();
      this.nav.push(LoginPage);
      this.nav.setRoot(LoginPage).then(data => {
        console.log(data);
      }, (error) => {
        console.log(error);
      })
    }, 3000);
  }

  openCheckout() {
    this.closeMyKart();
    if (this.cartservice.cart_count > 0) {
      this.nav.push(CheckoutSummaryPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Oops!',
        subTitle: 'add items to myKart before checkout',
        buttons: ['dismiss']
      });
      alert.present();
    }
  }
  openMyAccount(index) {
    console.log(index);
    this.nav.push(MyaccountPage, { 'index': index });
    this.menuCtrl.enable(false, 'mykart');
    this.menuCtrl.toggle();
  }
  openAboutUs() {
    this.nav.push(AboutUsPage);
  }
  openPrivacyPolicy() {
    this.nav.push(PrivacyPolicyPage);
  }
  openFaq() {
    this.nav.push(FaqPage);
  }

  launchExternalApp(iosSchemaName: string, androidPackageName: string, appUrl: string, httpUrl: string, username: string) {
    let app: string;
    if (this.device.platform === 'iOS') {
      app = iosSchemaName;
    } else if (this.device.platform === 'Android') {
      app = androidPackageName;
    } else {
      window.open(httpUrl + username, '_system');
      return;
    }

    this.appAvailability.check(app).then(
      () => { // success callback
        window.open(appUrl + username, '_system');
      },
      () => { // error callback
        window.open(httpUrl + username, '_system');
      }
    );
  }

  openInstagram(username: string) {
    this.launchExternalApp('instagram://', 'com.instagram.android', 'instagram://user?username=', 'https://www.instagram.com/', 'pushkartph');
  }

  openTwitter(username: string) {
    this.launchExternalApp('twitter://', 'com.twitter.android', 'twitter://user?screen_name=', 'https://twitter.com/', 'pushkartph');
  }

  openFacebook(username: string) {
    this.launchExternalApp('fb://', 'com.facebook.katana', 'fb://page/', 'https://www.facebook.com/', '994064437298552');
  }
}
