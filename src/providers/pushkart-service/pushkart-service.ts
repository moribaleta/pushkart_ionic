import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the PushkartServiceProvider provider.

  See https://angular.io/docs/ts/la/+user.guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()
export class PushkartServiceProvider {

  product_list: any;
  category: any;
  url: string = 'https://v3.pushkart.ph/dev3/';
  tax_value: number = 0.12;

  constructor(public http: Http) {

  }

  setUrl(url) {
    this.url = url;
  }

  static get parameters() {
    return [[Http]];
  }

  getCategories() {
    var url = this.url + 'angular_php/getCategory.php';
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getProductbyCategories(category, size) {
    var url = this.url + 'angular_php/getProductByCategory.php?catId=' + category + '&size=' + size;
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getItems() {
    var url = this.url + 'angular_php/getProducts.php';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getSlots() {
    var url = this.url + 'pushkart-customer/slot/booking';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  setSlot(id) {
    var url = this.url + 'slotbooking/index/book/id/' + id + '/week/1/popup/1/';
    console.log('url: ' + url);
    var response = this.http.get(url).map(res => res.text());
    return response;
  }
  
  selectPaypal() {
 
    let url = this.url + 'onepage/json/customSave/payment/paypal_express';
    console.log('url ni kaka :'+url);

    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  finalOrderCod(cart_items, user, couponcode, kredits) {
    var pid = '';
    var qty = '';
    for (var key in cart_items) {
      pid += cart_items[key].product_id + ',';
      qty += cart_items[key].qty + ',';
    }
    if (pid.charAt(pid.length - 1) == ',') {
      pid = pid.substring(0, pid.length - 1);
    }
    if (qty.charAt(qty.length - 1) == ',') {
      qty = qty.substring(0, qty.length - 1);
    }


    let url = this.url + 'pushkart-customer/test/cod/' +
      'email/' + user.email
      + '/pid/' + pid
      + '/pqty/' + qty
      + '/fname/' + user.firstname
      + '/lname/' + user.lastname
      + '/bfname/' + user.firstname
      + '/blname/' + user.lastname
      + '/bstreet/' + user.street
      + '/bvillage/' + user.village
      + '/bcity/' + user.city
      + '/bcountry/PH/bregion/Metro%20Manila/'
      + 'bpostcode/' + user.zipcode
      + '/btelephone/' + user.telephone
      + '/bmobile/' + user.mobile
      + '/sfname/' + user.firstname
      + '/slname/' + user.lastname
      + '/sstreet/' + user.street
      + '/svillage/' + user.village
      + '/scity/' + user.city
      + '/scountry/PH/sregion/Metro%20Manila/'
      + 'spostcode/' + user.zipcode
      + '/stelephone/' + user.telephone
      + '/smobile/' + user.mobile
      + '/slotid/' + user.slotid
      + '/shipid/' + user.shipid
      + '/couponcode/' + couponcode
      + '/kreds/' + kredits;

    console.log('url: ' + url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  finalOrder(cart_items, user, couponcode, kredits) {
    var pid = '';
    var qty = '';
    for (var key in cart_items) {
      pid += cart_items[key].product_id + ',';
      qty += cart_items[key].qty + ',';
    }
    if (pid.charAt(pid.length - 1) == ',') {
      pid = pid.substring(0, pid.length - 1);
    }
    if (qty.charAt(qty.length - 1) == ',') {
      qty = qty.substring(0, qty.length - 1);
    }

    let url = this.url + 'pushkart-customer/test/test/' +
      'email/' + user.email
      + '/pid/' + pid
      + '/pqty/' + qty
      + '/fname/' + user.firstname
      + '/lname/' + user.lastname
      + '/bfname/' + user.firstname
      + '/blname/' + user.lastname
      + '/bstreet/' + user.street
      + '/bvillage/' + user.village
      + '/bcity/' + user.city
      + '/bcountry/PH/bregion/Metro%20Manila/'
      + 'bpostcode/' + user.zipcode
      + '/btelephone/' + user.telephone
      + '/bmobile/' + user.mobile
      + '/sfname/' + user.firstname
      + '/slname/' + user.lastname
      + '/sstreet/' + user.street
      + '/svillage/' + user.village
      + '/scity/' + user.city
      + '/scountry/PH/sregion/Metro%20Manila/'
      + 'spostcode/' + user.zipcode
      + '/stelephone/' + user.telephone
      + '/smobile/' + user.mobile
      + '/slotid/' + user.slotid
      + '/shipid/' + user.shipid
      + '/couponcode/' + couponcode
      + '/kreds/' + kredits;

    console.log('url: ' + url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }


  paypalOrder() {

    let url = this.url + 'paypal/express/startmobile/';
    console.log('url: ' + url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  checkinc(inc) {
    var url = this.url + 'pushkart-app/status/checker/inc/' + inc;
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getOrderDetails(id) {
    var url = this.url + 'pushkart-app/status/orderdetails/inc/' + id;
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getKredit(id) {
    var url = this.url + 'pushkart-app/account/getkredit/ids/' + id;
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getInfo(email) {
    var url = this.url + 'pushkart-app/account/getinfo/email/' + email;
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  changeUserDetails(user) {
    var url = this.url + 'pushkart-app/account/cp/'
      + 'email/' + user.email
      + '/firstname/' + user.firstname
      + '/lastname/' + user.lastname
      + '/gender/' + user.gender
      + '/newpassword/' + user.new_password
      + '/oldpassword/' + user.password;
    console.log(url);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getOrderlist(id) {
    var url = this.url + 'pushkart-app/status/orderlist/cid/' + id;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  callCoupon(code) {
    var url = this.url + 'pushkart-customer/test/applycoupon/couponcode/' + code;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  cancelCoupon() {
    var url = this.url + 'pushkart-customer/test/cancelcoupon';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  callKredits() {
    var url = this.url + 'pushkart-app/account/kreditbalance';
    var response = this.http.get(url).map(res => res.text());
    return response;
  }

  cancelOrder(order) {
    var url = this.url + 'pushkart-customer/test/cancelorder/inc_id/' + order;
    var response = this.http.get(url).map(res => res.text());
    return response;
  }

}
