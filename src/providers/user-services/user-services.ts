import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserServicesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()
export class UserServicesProvider {
  public splash: Boolean = true;

  url: string = 'https://v3.pushkart.ph/dev3/';
  user_data: any;
  constructor(public http: Http, public storage: Storage) {
    this.getUser();
    this.getUserData();
  }

  setUrl(url){
    this.url = url;
  }

  user: string = '';
  email: string = '';
  password: string = '';
  user_details: any;

  saveUser(user) {
    this.user = user;
    this.storage.set('user', this.user);
    console.log('user login: ' + this.user);
  }
  savePassword(password){
    this.password = password;
    this.storage.set('password', password);
  }
  getPassword(){
    this.storage.get('password').then((val) => {
      this.password = val;
    });
  }
  saveEmail(email) {
    this.email = email;
    this.storage.set('email', this.email);
    console.log('saving: ' + email);
  }
  getEmail() {
    this.storage.get('email').then((val) => {
      this.email = val;
      console.log('email: ' + val);
    });
  }

  saveUserData(user_data){
    this.user_data = user_data;
    user_data = JSON.stringify(user_data);
    this.storage.set('user_data',user_data);
  }
  getUserData(){
    this.storage.get('user_data').then((val)=>{
      this.user_data = JSON.parse(val);
    }); 
  }

  logout() {
    this.user = '';
    this.user_details = {
      firstname: '',
      lastname: '',
      address: '',
      zone: '',
      city: '',
      telephone: '',
      mobile: '',
    }
    this.storage.set('user', '');
    this.storage.set('password','');
    this.storage.set('email', '');
  }

  getUser() {
    this.storage.get('user').then((val) => {
      this.user = val;
      console.log('user_id: ' + this.user);
      this.getEmail();
    });
    return this.user;
  }
  loginUser(){
    this.getEmail();
    this.getPassword();
    var user = {
      email: this.email,
      password: this.password
    }
    this.login(user).subscribe(
      data=>{
        console.log('server login success');
        console.log(data);
      },err=>{
        console.log('server login error \n'+err);
      }
    )
  }

  login(user) {
    var url = this.url+'pushkart-customer/login/customer/email/' + user.email + '/password/' + user.password;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  signup(user) {
    var url = this.url+'pushkart-customer/login/signup/firstname/' + user.firstname + '/lastname/' + user.lastname + '/email/' + user.email + '/birthday/' + user.date + '/gender/' + user.gender + '/password/' + user.password + '/mobile/' + user.mobile;
    console.log('url: ' + url);
    var response = this.http.post(url, {}).map(res => res.json());
    return response;
  }

  forgot(email) {
    var url = this.url+'pushkart-app/account/forgotter/email/'+email;
    var response = this.http.post(url, {}).map(res => res.text());
    return response;
  }



  getUserAddressOnline() {
    var url = this.url+'pushkart-app/index/getaddress/c_id/' + this.user;
    console.log(url);
    var response = this.http.post(url, {}).map(res => res.json());
    return response;
  }

  setAddress(user_details, newAddress) {
    this.user_details = user_details;
    if (newAddress) {
      this.createAddress(user_details).subscribe(
        data => {
          console.log('address data: %O', data);
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.editAddress(this.user_details).subscribe(
        data => {
          console.log('address data: %O', data);
        },
        err => {
          console.log(err);
        }
      );
    }
  }

  createAddress(user) {
    console.log('create user address: %o', user);
    var url = this.url+'pushkart-app/index/createaddress/'
      + 'email/' + this.email
      + '/fname/' + user.firstname
      + '/lname/' + user.lastname
      + '/company/glimsol/'
      + 'street/' + user.street
      + '/village/' + user.village
      + '/city/' + user.city
      + '/country_id/PH/region/metro%20manila/'
      + 'postal/' + user.postcode
      + '/telephone/' + user.telephone
      + '/mobile/' + user.mobile;
    console.log('user_details address: ' + url);
    var response = this.http.post(url, {}).map(res => res.text());
    return response;
  }
  editAddress(user) {
    console.log('edit user address: %o', user);
    var url = this.url+'pushkart-app/index/editaddress/'
      + 'id/' + user.entity_id
      + '/postcode/' + user.postcode
      + '/country_id/PH'
      + '/fname/' + user.firstname
      + '/lname/' + user.lastname
      + '/company/pushkart'
      + '/street/' + user.street
      + '/village/  ' + user.village
      + '/city/' + user.city
      + '/region/Metro%20Manila'
      + '/telephone/' + user.telephone
      + '/mobile/' + user.mobile;
    console.log('edit user address url: '+url);
    var response = this.http.post(url, {}).map(res => res.text());
    return response;
  }
  deleteAddress(id) {
    var url = this.url+'pushkart-app/index/deleteaddress/id/' + id;
    var response = this.http.post(url, {}).map(res => res.text());
    return response;
  }
}
