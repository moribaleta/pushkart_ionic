import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the thisProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
//  @Injectable()
export class CartserviceProvider {
  public url: string = 'https://v3.pushkart.ph/dev3/';
  public cart_product_loaded: Boolean = false;
  public cart: {};
  public cart_loaded: Boolean = false;
  public cart_saved: Boolean = false;
  public cart_count: number = 0;
  public cart_display_item: any;
  public cart_display_item_new: any;
  public total;
  public sample: any;
  public grocersfee: number = 199;
  public grocersmessage: any;
  public vacodchecker: any;

  public total_service;
  public service: any;
  public grocerspercent: string = '0%';
  public grocercolor: string = '#46b518';
  public user: string;
  
  constructor(public http: Http, public storage: Http) { }
  
  initialize() {
    this.cart = {};
    this.cart_display_item = [];
    this.cart_display_item_new = [];
    this.getPromo();
  }

  setUrl(url) {
    this.url = url;
  }

  display_cart() {
    console.log('display_cart log %o', this.cart);
  }

  static get parameters() {
    return [[Http]];
  }

  getTotal() {
    this.total = 0;
    this.cart_count = 0;
    for (var key in this.cart_display_item) {
      this.total += this.cart_display_item[key].price * this.cart_display_item[key].qty;
      this.cart_count += Number(this.cart_display_item[key].qty);
    }
    this.total = this.total.toFixed(2);
    this.getMessage(this.total);
    this.total_service = 0;
    this.total_service = Number(this.total) + Number(this.grocersfee);
    this.total_service = this.total_service.toFixed(2);
    console.log('cart count: ' + this.cart_count);
    this.grocerspercent = ((Number(this.total) / Number(this.service[2].price)) * 100).toString();
    this.grocerspercent += '%';
    console.log('grocers percent: ' + this.grocerspercent);

  }

  computePriceWPaypal(unitprice,qty,tots) {
    var total2 = Number(tots);
    var total = (Number(unitprice) * Number(qty))+total2;
    var string = total.toFixed(2);
    return string;
  }

  computePrice(unitprice, qty) {
    var total = Number(unitprice) * Number(qty);
    var string = total.toFixed(2);
    return string;
  }

  computeWeight(weight) {
    var total = Number(weight);
    var string = total.toFixed(2);
    string = string + "";
    var split = string.split(".");
    if (Number(split[1]) == 0) {
      return split[0];
    } else {
      return string;
    }
  }

  numberWithCommas(x) {
    try {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    } catch (err) {
      return x;
    }
  }

  getMessage(total) {
    if (Number(total) < Number(this.service[1].price)) {
      this.grocersfee = this.service[0].discount;
      var diff = Number(this.service[1].price) - Number(total);
      this.grocersmessage = 'Add ₱' + this.computePrice(diff, 1) + ' more to get ₱150 off your grocers fee';
    }
    else if (Number(total) >= Number(this.service[1].price) && Number(total) < Number(this.service[2].price)) {
      this.grocersfee = this.service[1].discount;
      var diff = Number(this.service[2].price) - Number(total);
      this.grocersmessage = 'Add ₱' + this.computePrice(diff, 1) + ' more to your kart to waive your grocers fee';
    } else {
      this.grocersfee = this.service[2].discount;
      this.grocersmessage = 'Awesome! We\'ve waived your grocer\'s fee!!!';
    }
  }

  addItemProduct(item) {

    this.cart[item.entity_id].qty++;
    console.log('this.cart: %o', this.cart);
    console.log('item added: %o', this.cart[item.entity_id]);
    item['product_id'] = item.entity_id;
    let exist = false;
    for (var key in this.cart_display_item) {
      if (this.cart_display_item[key].product_id == item.entity_id) {
        this.cart_display_item[key].qty = this.cart[item.entity_id].qty;
        exist = true;
        break;
      }
    }
    if (!exist) {
      var product = {
        cart_id: null,
        item_id: null,
        price: item.price,
        product_id: item.entity_id,
        product_name: item.name,
        qty: this.cart[item.entity_id].qty,
        sku: item.sku,
        weight: item.weight,
        weight_unit: item.weight_unit
      };
      this.cart_display_item.push(product);
      this.addItem(item.entity_id, 1);
    }
    this.cart_count++;
    this.getTotal();
  }

  updateQty(value, qty) {
    this.cart[value.entity_id].qty = qty;
    for (var key in this.cart_display_item) {
      if (this.cart_display_item[key].product_id == value.entity_id) {
        this.cart_display_item[key].qty = qty;
        break;
      }
    }
    if (qty == 0) {
      this.decreaseQty(value);
    }
    this.getTotal();
    this.getCartCount();
  }

  getCartCount() {
    this.cart_count = 0;
    for (var key in this.cart) {
      this.cart_count += Number(this.cart[key].qty);
    }
  }

  increaseQtyProduct(item) {
    console.log('increaseQtyProduct item: %o', item);
    this.cart[item.entity_id].qty++;
    for (var key in this.cart_display_item) {
      if (this.cart_display_item[key].product_id == item.entity_id) {
        this.cart_display_item[key].qty++;
        break;
      }
    }
    this.cart_count++;
  }


  increaseQtyKart(id) {
    this.cart[id].qty++;
    for (var key in this.cart_display_item) {
      if (this.cart_display_item[key].product_id == id) {
        this.cart_display_item[key].qty++;
        break;
      }
    }
    console.log('item updated' + id + ' qty ' + this.cart[id].qty);
    this.cart_count++;
    this.getTotal();
  }

  decreaseQtyKart(item) {
    this.cart[item.product_id].qty--;
    if (this.cart[item.product_id].qty <= 0) {
      this.cart[item.product_id].qty = 0;
      var index = 0;
      for (var key in this.cart_display_item) {
        if (this.cart_display_item[key].product_id == item.product_id) {
          break;
        }
        index++;
      }
      this.cart_display_item.splice(index, 1);
      this.removeItem(item.item_id);
    } else {
      for (var key in this.cart_display_item) {
        if (this.cart_display_item[key].product_id == item.product_id) {
          this.cart_display_item[key].qty = this.cart[item.product_id].qty;
          break;
        }
      }
    }
    this.cart_count--;
    this.getTotal();
  }

  removeKartQty(item) {
    console.log('item %o', this.cart[item.product_id]);
    var qty = this.cart[item.product_id].qty;
    this.cart[item.product_id].qty = 0;
    this.cart_count -= qty;
    this.cart[item.product_id].qty = 0;
    console.log('remove item', this.cart[item.product_id]);
    var index = 0;
    for (var key in this.cart_display_item) {
      if (this.cart_display_item[key].product_id == item.product_id) {
        break;
      }
      index++;
    }
    this.cart_display_item.splice(index, 1);
    this.removeItem(item.item_id);
    this.getTotal();
  }

  decreaseQty(item) {
    try {
      this.cart[item.product_id].qty--;
    } catch (err) {
      this.cart[item.entity_id].qty--;
    }
    if (this.cart[item.product_id].qty <= 0) {
      this.cart[item.product_id].qty = 0;
      var index = 0;
      for (var key in this.cart_display_item) {
        if (this.cart_display_item[key].product_id == item.product_id) {
          break;
        }
        index++;
      }
      this.cart_display_item.splice(index, 1);
      this.removeItem(item.item_id);
    } else {
      for (var key in this.cart_display_item) {
        if (this.cart_display_item[key].product_id == item.product_id) {
          this.cart_display_item[key].qty = this.cart[item.product_id].qty;
          break;
        }
      }
    }
    this.cart_count--;
    this.getTotal();
  }

  dataDump() {
      this.saveCart();
      setTimeout(() => {
        this.cart_display_item = [];
        for (var key in this.cart) {
          this.cart[key].qty = 0;
        }
      }, 5000);
  }
  
  saveCart() {
    for (var key in this.cart_display_item) {
      var item = this.cart_display_item[key];
      this.updateItem(item.product_id, this.cart[item.product_id].qty);
    }
  }

  clearCart() {
    this.cart_display_item_new = this.cart_display_item;
    var url = this.url + 'custom/ajax/clearclear/';
    this.http.post(url, {}).subscribe(
      data => {
        console.log('clear cart %o',data);
      },
      error => {
        console.log(error);
      });
      
    setTimeout(() => {
      this.cart_display_item = [];
      for (var key in this.cart) {
        this.cart[key].qty = 0;
      }
    }, 5000);
  }

  clearLogout(){
    this.cart_display_item_new = this.cart_display_item;
    var url = this.url + 'custom/ajax/clearclear/';
    this.http.post(url, {}).subscribe(
      data => {
        console.log('clear logout %o',data);
        this.addAllItems();
      },
      error => {
        console.log(error);
      });
      
    setTimeout(() => {
      this.cart_display_item = [];
      for (var key in this.cart) {
        this.cart[key].qty = 0;
      }
    }, 5000);
  }

  addAllItems(){
    console.log('items to',  JSON.stringify(this.cart_display_item_new));
    var link = this.url + 'custom/ajax/ganap/';
    var data = JSON.stringify(this.cart_display_item_new);

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    
    this.http.post(link, data, options)
      .subscribe(data => {
        console.log(data['_body']);
       }, error => {
        console.log(error);
      });
  }

  updateItem(id, qty) {
    var url = this.url + 'custom/update/cart2/product_id/' + id + '/qty/' + qty;
    this.http.get(url).subscribe(
      data => {
        console.log(data);
        //this.finishedSaving();
        //this.getCart();
      },
      error => {
        console.log(error);
      });

  }

  addItem(id, qty) {
    var url = this.url + 'pushkart-customer/cart/add/product/' + id + '/cs_id/' + this.user + '?options=cart';
    this.http.post(url, {}).subscribe(
      data => {
        this.getCartByUser(this.user);
      },
      error => {
        console.log(error);
      });
  }



  getPromoData() {
    var url = this.url + 'angular_php/getPromo.php';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  getPromo() {
    this.getPromoData().subscribe(
      data => {
        this.service = data;
        console.log('service %o', this.service);
        this.getTotal();
      },
      err => {
        console.log(err);
      }
    )
  }

  getCartByUser(user) {
    this.getCartByUserData(user).subscribe(
      data => {
        console.log('cart data by user %o', data);

        this.cart_count = 0;
        for (var key in data) {
          try {
            data[key].small_image = this.cart[data[key].product_id].small_image;
          } catch (err) {

          }
          var exist = false;
          for (var key_2 in this.cart_display_item) {
            if (data[key].product_id == this.cart_display_item[key_2].product_id) {
              this.cart_display_item[key_2].cart_id = data[key].item_id;
              exist = true;
              break;
            }
          }
        }
        console.log('cart update: %o', this.cart_display_item);
        this.getTotal();
      },
      err => {
        console.log(err);
      }
    )
  }



  getCartByUserData(user) {
    var url = this.url + 'pushkart-customer/cart/index/cs_id/' + user;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  clearkart() {
    for (var key in this.cart_display_item) {
      var item = this.cart_display_item[key];
      this.cart[item.product_id].qty = 0;
      this.removeItem(item.item_id);
    }
    this.cart_count = 0;
    this.cart_display_item = [];
    this.getTotal();
    //this.cart_display = this.cart;
  }
  

  removeItem(id) {
    var url = this.url + 'pushkart-customer/cart/delete/id/' + id;
    this.http.get(url).subscribe(
      data => {
        console.log('removeitem message: %o', data);
       },
      error => {
        console.log(error);
      });
  }

  codchecker()
  {

  
    var url = this.url + 'pushkart-customer/index/codchecker/';
    this.http.get(url).subscribe(
      data => {

        this.vacodchecker = data.text();
        console.log(this.vacodchecker+" testnga");
         //return returner;   
       });
  }

}
