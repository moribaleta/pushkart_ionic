import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-services/user-services';

@IonicPage()
@Component({
  selector: 'page-forgotpass',
  templateUrl: 'forgotpass.html',
})
export class ForgotpassPage {
  buttonclass: string = 'not-active';
  email: string = '';
  constructor(
    private userService: UserServicesProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpassPage');
  }

  resetpassword() {
    if (this.email != '' && this.email.length > 10) {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.userService.forgot(this.email).subscribe(
        data => {
          console.log('signup message: %o', data);
          loading.dismiss();
          let alert = this.alertCtrl.create({
            title: 'forgot password',
            subTitle: data,
            buttons: ['dismiss  ']
          });
          alert.present();
        },
        err => {
          loading.dismiss();
          console.log(err);
        }
      )

    } else {
      let toast = this.toastCtrl.create({
        message: 'email not valid',
        duration: 2000,
        position: 'bottom'
      });
      toast.onDidDismiss(() => {
       // this.reset();
      });
      toast.present();
    }
  }

  checkAll() {
    console.log(this.email);
    if (this.email != '') {
      this.buttonclass = 'active';
    }
    else {
      this.buttonclass = 'not-active';
    }
  }


}
