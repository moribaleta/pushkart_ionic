import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { PaymentPage } from '../../pages/payment/payment';
import { ModalAddressPage } from '../../pages/modal-address/modal-address';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the CheckoutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
/* @IonicPage() */
@Component({
  selector: 'checkout-page',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  user_details: any;
  cities: any;
  modeKeys: any;
  date_sched: Number;
  time_sched: Number;
  delivery_date: any;
  delivery_time: any;
  slotsLoaded: boolean = false;
  paymaya_card: Boolean = false;
  dragon_card: Boolean = false;
  paypal_card: Boolean = false;
  cod: Boolean = false;
  bypass: Boolean = true;

  card_payment: Number = 4;

  loaded_address: boolean = false;
  group_address: any;
  user_address: string;
  viewaddress: boolean = false;
  editaddress: boolean = false;
  newaddress: boolean = false;
  showaddress_button: string = 'show address';
  address_view_drop: string = 'arrow-dropdown';
  createaddress_button: string = 'add new address';
  earnkredits: any;
  userkredits: number = 0;
  kreditinput: number = 0;
  couponcode: string;
  total_value: any;
  discount_voucher: any;
  discount_kredit: any;
  paypal_charge:any;
  codcondition:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cartservice: CartserviceProvider,
    public pushkartService: PushkartServiceProvider,
    public toastCtrl: ToastController,
    public userService: UserServicesProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController
  ) {
    this.total_value = cartservice.total_service;
    this.getKreditValue();
    this.group_address = [];
    this.initValues();
    this.getSlots();
    this.getAddresses();

  }

  initValues() {
    this.user_details = {
      firstname: '',
      lastname: '',
      village: '',
      street: '',
      postcode: '',
      city: '',
      telephone: '',
      mobile: '',
    }
    this.cities = [
      'Caloocan'
      , 'Las Pinas'
      , 'Makati'
      , 'Malabon'
      , 'Mandaluyong'
      , 'Manila'
      , 'Marikina'
      , 'Muntinlupa'
      , 'Navotas'
      , 'Paranaque'
      , 'Pasay'
      , 'Pasig'
      , 'Pateros'
      , 'Quezon City'
      , 'San Juan'
      , 'Taguig'
      , 'Valenzuela'
    ];
    this.delivery_date = [
      { month: 'JUL', day: '31', weekday: 'MON', class: 'input-text delivery-date not-active' },
      { month: 'AUG', day: '01', weekday: 'TUE', class: 'input-text delivery-date not-active' },
      { month: 'AUG', day: '02', weekday: 'WED', class: 'input-text delivery-date not-active' },
      { month: 'AUG', day: '03', weekday: 'THU', class: 'input-text delivery-date not-active' },
      { month: 'AUG', day: '04', weekday: 'FRI', class: 'input-text delivery-date not-active' }
    ];
    this.delivery_time = [
      { time: '09:00 am - 11:00 am', class: 'input-text delivery-time not-active' },
      { time: '11:00 am - 01:00 pm', class: 'input-text delivery-time not-active' },
      { time: '01:00 pm - 04:00 pm', class: 'input-text delivery-time not-active' },
      { time: '04:00 pm - 06:00 pm', class: 'input-text delivery-time not-active' },
      { time: '06:00 pm - 08:00 pm', class: 'input-text delivery-time not-active' }
    ];
  }

  getKreditValue() {
    this.earnkredits = this.cartservice.total_service;
    if (this.earnkredits >= 1000) {
      this.earnkredits = (this.earnkredits / 1000);
      this.earnkredits = ~~this.earnkredits;
    } else {
      this.earnkredits = 0;
    }
    this.discount_voucher = {
      'voucher_show': false,
      'voucher_code': '',
      'voucher_value': 0,
    };
    this.discount_kredit = {
      'kredit_show': false,
      'kredit_code': '',
      'kredit_value': 0
    };
    this.pushkartService.callKredits().subscribe(
      data => {
        console.log('kredit: ' + data);
        if (data != 'Failed') {
          this.userkredits = Number(data);
        } else {
          this.userkredits = 0;
        }
      }, err => {
        console.log(err);
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
    this.paypal_charge = 0;
    if(this.cartservice.vacodchecker == 'yes')
    {
      this.codcondition = true;
    }
   // this.cartservice.codchecker();
    
  }

  codhide()
  {
 //   if(this.cartservice.codchecker() == "yes")
   // {

    //}
   
  }

  voucher_toggle() {
    this.discount_voucher.voucher_show = !this.discount_voucher.voucher_show;
  }

  kredit_toggle() {
    this.discount_kredit.kredit_show = !this.discount_kredit.kredit_show;
  }

  showAddress() {
    this.viewaddress = !this.viewaddress;
    if (this.viewaddress) {
      this.showaddress_button = 'close addresses';
      this.address_view_drop = 'arrow-dropup';
    } else {
      this.showaddress_button = 'show address';
      this.address_view_drop = 'arrow-dropdown';
    }
  }

  createAddress() {
    let modal = this.modalCtrl.create(ModalAddressPage);

    modal.onDidDismiss(data => {
      try {
        this.user_details = data.user_details;
        this.user_details.class = 'address-item not-active';
        this.group_address.push(data.user_details);
        console.log(data.user_details);
      } catch (err) {

      }
    })
    modal.present();
  }

  getAddresses() {
    this.userService.getUserAddressOnline().subscribe(
      data => {
        this.loaded_address = true;
        for (var key in data) {
          data[key].class = 'address-item not-active';
        }
        this.group_address = data;
        console.log('addresses : %o', data);
      },
      err => {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          message: "Can't get your address",
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
                this.navCtrl.pop();
              }
            },
            {
              text: 'Reload',
              handler: () => {
                //console.log('Buy clicked');
                this.getAddresses();
              }
            }
          ]
        });
        alert.present();
      }
    )
  }

  openTermsAndCondition() {
    this.navCtrl.push(TermsandconditionsPage);
  }
  openPrivacyPolicy() {
    this.navCtrl.push(PrivacyPolicyPage);
  }

  editAddress(address, i) {
    this.selectAddress(address, i);
    var modal = this.modalCtrl.create(ModalAddressPage, { edit: true, address: address });
    modal.onDidDismiss(data => {
      try {
        this.group_address[i] = data.user_details;
      } catch (err) {

      }
    });
    modal.present();
    console.log("edit");

  }

  deleteAddress(address, i) {
    this.selectAddress(address, i);
    console.log('delete ' + i);
    this.userService.deleteAddress(this.group_address[i].entity_id).subscribe(
      data => {
        console.log('data: %o', data);

        let toast = this.toastCtrl.create({
          message: 'deleted address',
          duration: 1000,
          position: 'bottom'
        });
        toast.present();
      }, err => {
        console.log(err);
      }
    )
    this.group_address.splice(i, 1);
  }

  selectAddress(address, index) {
    for (var i = 0; i < this.group_address.length; i++) {
      if (index == i) {
        this.group_address[i].class = 'address-item selected';
      } else {
        this.group_address[i].class = 'address-item not-active';
      }
    }
    this.user_details = address;
    console.log('address set: ' + this.user_details);
  }

  updateCard(val) {
    if (val == 1) {
      this.dragon_card = false;
      this.paypal_card = false;
      this.bypass = false;
      this.paymaya_card = true;
      this.cod = false;
      this.paypal_charge = 0;
    } else if (val == 2) {
      this.paymaya_card = false;
      this.paypal_card = false;
      this.bypass = false;
      this.dragon_card = true;
      this.cod = false;
      this.paypal_charge = 0;
    } else if (val == 3) {
      this.dragon_card = false;
      this.paymaya_card = false;
      this.bypass = false;
      this.paypal_card = true;
     
      this.paypal_charge =  (this.cartservice.total*0.07).toFixed(2);

      this.cod = false;

    }
    else if (val == 4)
    {
      this.paypal_charge = 0;
      this.dragon_card = false;
      this.paymaya_card = false;
      this.bypass = false;
      this.paypal_card = false;
      this.cod = true;
    } 
    
    else {
      this.dragon_card = false;
      this.paypal_card = false;
      this.paymaya_card = false;
    }
    this.card_payment = val;
  }

  setDate(index, id) {
    console.log(index);
    for (var key in this.delivery_date) {
      this.delivery_date[key].class = 'input-text delivery-date not-active';
    }
    this.delivery_date[index].class = 'input-text delivery-date active selected';
    this.date_sched = id;
    this.delivery_time = this.delivery_date[index].time;
    console.log(this.delivery_date[index].time);
    this.time_sched = null;
    for (var key in this.delivery_time) {
      if (this.delivery_time[key].class.indexOf("not-active") == -1) {
        this.delivery_time[key].class = 'input-text delivery-time active';
      }
    }
  }

  setTime(index, id) {
    console.log(index);
    var class_ = this.delivery_time[index].class;
    if (class_.split(' ')[2] != 'not-active') {
      for (var key in this.delivery_time) {
        if (this.delivery_time[key].class == 'input-text delivery-time active' || this.delivery_time[key].class == 'input-text delivery-time active selected') {
          this.delivery_time[key].class = 'input-text delivery-time active';
        } else {
          this.delivery_time[key].class = 'input-text delivery-time not-active';
        }
      }
      this.delivery_time[index].class = 'input-text delivery-time active selected';
      this.time_sched = id;
      console.log('time_sched set: ' + this.time_sched);
    } else {
      let toast = this.toastCtrl.create({
        message: 'slot not available',
        duration: 1000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  proceed() {
    if (this.time_sched != null) {
      if (this.card_payment == 1 || this.card_payment == 4 || this.card_payment == 3) {
        let loading = this.loadingCtrl.create({
          content: 'processing order \n please wait...'
        });
          
        loading.present();
        this.pushkartService.setSlot(this.time_sched).subscribe(
          data => {
            loading.dismiss();
            this.setOrder();
          }, err => {
            console.log(err);
            loading.dismiss();
            this.setOrder();
          }
        )
      } else {
        let alert = this.alertCtrl.create({
          title: 'error',
          subTitle: 'payment method not available',
          buttons: ['dismiss']
        });
        alert.present();
      }
    } else {
      let alert = this.alertCtrl.create({
        title: 'error',
        subTitle: 'please select a time of delivery',
        buttons: ['dismiss']
      });
      alert.present();
    }
  }

  setOrder() {
    var user = {
      email: this.userService.email,
      firstname: this.user_details.firstname,
      lastname: this.user_details.lastname,
      street: this.user_details.street,
      village: this.user_details.village,
      city: this.user_details.city,
      zipcode: this.user_details.postcode,
      telephone: this.user_details.telephone,
      mobile: this.user_details.mobile,
      slotid: this.time_sched,
      shipid: this.cartservice.grocersfee
    }
    if (
      user.email != '' && user.firstname != '' &&
      user.lastname != '' && user.street != '' &&
      user.village != '' && user.city != '' &&
      user.zipcode != '' && user.telephone != '' &&
      user.mobile != ''
    ) {
      let loading = this.loadingCtrl.create({
        content: 'processing order \n please wait...'
      });
      if(this.cod == true)
      {
        loading.present();
        console.log('user %o', user);
        this.pushkartService.finalOrderCod(this.cartservice.cart_display_item, user, this.couponcode, this.kreditinput).subscribe(
          data => {
            loading.dismiss();
            console.log('message: %o', data);
            //this.navCtrl.push(PaymentPage, { 'url': data.plink, 'inc': data.inc });
            //this.navCtrl.push(PaymentPage, { 'url': data });
            let message = 'something happened, try again later';
            let success = false
            try {
              if (data.plink != null && data.inc != '') {
                message = 'order success'
                success = true;
              }
            } catch (err) {
  
            }
  
            if (success) {
              /* let alert = this.alertCtrl.create({
                title: 'Order',
                subTitle: message,
                buttons: ['dismiss']
              });
              alert.present(); */
              this.cartservice.clearCart();
              this.cartservice.clearLogout();
              this.cartservice.clearkart();
              
              this.cartservice.cart_count = 0;


              
              this.navCtrl.push('PaymentsuccessPage', {orderId: data.inc});
            } else {
              let alert = this.alertCtrl.create({
                title: 'Order',
                subTitle: message,
                buttons: ['dismiss']
              });
              alert.present();
            }
  
          },
          err => {
            loading.dismiss();
            console.log(err);
            /* let toast = this.toastCtrl.create({
              message: 'something happened',
              duration: 1000,
              position: 'bottom'
            });
            toast.present(); */
            let alert = this.alertCtrl.create({
              title: 'Oops! something happened',
              subTitle: 'the slot is already taken',
              buttons: ['dismiss']
            });
            alert.present();
          }
        )
        
      }
      else if(this.paypal_card == true)
      {
        loading.present();
        console.log('user %o', user);
        this.pushkartService.selectPaypal();

        this.pushkartService.paypalOrder().subscribe(
          data => {
            loading.dismiss();
            console.log('message: %o', data);
            //this.navCtrl.push(PaymentPage, { 'url': data.plink, 'inc': data.inc });
            //this.navCtrl.push(PaymentPage, { 'url': data });
            let message = 'something happened, try again later';
            let success = false
            try {
              if (data.plink != null && data.inc != '') {
                message = 'order success proceed to payment'
                success = true;
                console.log('test '+data);

              }
            } catch (err) {
  
            }
  
            if (success) {
              console.log('pasok');
              console.log('url mo '+ data.plink);

              this.navCtrl.push(PaymentPage, { 'url': data.plink, 'inc': data.inc });
            } else {
              let alert = this.alertCtrl.create({
                title: 'Order',
                subTitle: message,
                buttons: ['dismiss']
              });
              alert.present();
            }
  
          },
          err => {
            loading.dismiss();
            console.log(err);
            /* let toast = this.toastCtrl.create({
              message: 'something happened',
              duration: 1000,
              position: 'bottom'
            });
            toast.present(); */
            let alert = this.alertCtrl.create({
              title: 'Oops! something happened',
              subTitle: 'the slot is already taken',
              buttons: ['dismiss']
            });
            alert.present();
          }
        )
        
          // this.navCtrl.push(SuccesspagePage);
      }
      else
      {
        loading.present();
        console.log('user %o', user);
        this.pushkartService.finalOrder(this.cartservice.cart_display_item, user, this.couponcode, this.kreditinput).subscribe(
          data => {
            loading.dismiss();
            console.log('message: %o', data);
            //this.navCtrl.push(PaymentPage, { 'url': data.plink, 'inc': data.inc });
            //this.navCtrl.push(PaymentPage, { 'url': data });
            let message = 'something happened, try again later';
            let success = false
            try {
              if (data.plink != null && data.inc != '') {
                message = 'order success proceed to payment'
                success = true;
              }
            } catch (err) {
  
            }
  
            if (success) {
              this.navCtrl.push(PaymentPage, { 'url': data.plink, 'inc': data.inc });
            } else {
              let alert = this.alertCtrl.create({
                title: 'Order',
                subTitle: message,
                buttons: ['dismiss']
              });
              alert.present();
            }
  
          },
          err => {
            loading.dismiss();
            console.log(err);
            /* let toast = this.toastCtrl.create({
              message: 'something happened',
              duration: 1000,
              position: 'bottom'
            });
            toast.present(); */
            let alert = this.alertCtrl.create({
              title: 'Oops! something happened',
              subTitle: 'the slot is already taken',
              buttons: ['dismiss']
            });
            alert.present();
          }
        )
      }
     
    } else {
      let alert = this.alertCtrl.create({
        title: 'Oops! something happened',
        subTitle: 'Please select an address before proceeding',
        buttons: ['dismiss']
      });
      alert.present();
    }
  }

  getSlots() {

    var date = [];
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    this.pushkartService.getSlots().subscribe(
      data => {
        console.log('date slot: %o', data);
        date = data;
        //date = message;

        this.delivery_date = [];
        for (var key in date) {
          var d = new Date(date[key].dates);
          var final_month = d.getMonth();
          var final_date = d.getDate();
          console.log('month'+final_month);
          console.log('date'+final_date);
          var timeslots = [];
          try {
            console.log('timeslot available:' + date[key].time.length);
            for (var i = 0; i < date[key].time.length; i++) {
              timeslots.push({ 'time': date[key].time[i], 'id': date[key].id[i], class: 'input-text delivery-time active', cutoff: date[key].cutoff[i] });
            }
          } catch (err) {
          }

          var id = null;
          if (date[key].id != undefined) {
            id = date[key].id;
          }
          console.log('time dev entry %o', timeslots);
          var date_input = {
            month: month[final_month],
            day: final_date,
            id: id,
            weekday: days[d.getDay()],
            class: 'input-text delivery-date not-active',
            time: timeslots
          };
          this.delivery_date.push(date_input);
        }
        this.slotsLoaded = true;
        console.log('slots loaded: ' + this.slotsLoaded);
        this.getCutOff();
        this.getAvailableSlots();
      },
      err => {
        console.log(err);
        let alert = this.alertCtrl.create({
          title: 'Sorry',
          message: "Can't get available slots right now",
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                //console.log('Cancel clicked');
                this.navCtrl.pop();
              }
            },
            {
              text: 'Reload',
              handler: () => {
                //console.log('Buy clicked');
                this.getSlots();
              }
            }
          ]
        });
        alert.present();
      }
    )
  }
  getCutOff() {
    /* var date = this.delivery_date[0];
    var start = ['7:00 AM', '9:00 AM', '12:00 PM', '2:00 PM'];
    var slot = date.time.length;
    var startTime = '1:00 AM';
    for (var i in date.time) {
      var endTime = start[i];
      var now = new Date();
      var startDate = this.dateObj(startTime);
      var endDate = this.dateObj(endTime);
      var open = 'not-active';
      if(now < endDate && now > startDate){
        open = 'active';
      }
      console.log('start time:' + startTime + 'end time' + endTime + 'curr time' + now + 'time is ' + open);
      date.time[i].class = 'input-text delivery-time ' + open;
      console.log('change time: %o', date.time);
      if (open == 'not-active') {
        slot--;
      }
    }
    console.log('change date: %o', date);
    this.delivery_date[0] = date;
    console.log('change delivery_date: %o', this.delivery_date[0]);
    if (slot <= 0 || now > this.dateObj('2:00 PM')) {
      this.delivery_date.splice(0, 1);
    } */
    var date = this.delivery_date[0];
    var slot = 0;
    var now = new Date();
    var currHour = now.getHours();
    var am_pm = now.getHours() >= 12 ? "PM" : "AM";
    if (Number(currHour) >= 7 && am_pm == 'AM' || am_pm == 'PM') {
      date.time[0].class = 'input-text delivery-time ' + 'not-active';
      slot--;
    } else {
      date.time[0].class = 'input-text delivery-time ' + 'active';
    }
    if (Number(currHour) >= 9 && am_pm == 'AM' || am_pm == 'PM') {
      date.time[1].class = 'input-text delivery-time ' + 'not-active';
      slot--; 
    } else {
      date.time[1].class = 'input-text delivery-time ' + 'active';
    }
    if (Number(currHour) >= 12 && am_pm == 'PM') {
      date.time[2].class = 'input-text delivery-time ' + 'not-active';
      slot--;
    } else {
      date.time[2].class = 'input-text delivery-time ' + 'active';
    }
    if (Number(currHour) >= 14 && am_pm == 'PM') {
      date.time[3].class = 'input-text delivery-time ' + 'not-active';
      slot--;
    } else {
      date.time[3].class = 'input-text delivery-time ' + 'active';
    }
    this.delivery_date[0] = date;
    if (slot <= 0 && now > this.dateObj('2:00 PM')) {
      this.delivery_date.splice(0, 1);
    }
    console.log('current'+Number(currHour));
  }

  dateObj(d) {
    var parts = d.split(/:|\s/),
      date = new Date();
    if (parts.pop().toLowerCase() == 'pm') parts[0] = (+parts[0]) + 12;
    date.setHours(+parts.shift());
    date.setMinutes(+parts.shift());
    return date;
  }

  getAvailableSlots() {
    var date = this.delivery_date[0];
    this.setDate(0, date.id);
    /* var time = date.time[0];
    this.setDate(0, date.id);
    this.setTime(0, time.id); */
    for (var i = 0; i < date.time.length; i++) {
      if (date.time[i].class.split(' ')[2] == 'active') {
        this.setTime(i, date.time[i].id);
        break;
      }
    }
  }



  useCoupon() {
    var condition = this.discount_voucher.voucher_code.replace(/^\s\s*/, '');
    console.log(condition.length);
    if(!condition.length){
      console.log('blank coupon');
    }
    else{
      let loading = this.loadingCtrl.create({
        content: 'processing\nplease wait...'
      });
      loading.present();
      this.pushkartService.callCoupon(this.discount_voucher.voucher_code).subscribe(
        data => {
          console.log('coupon message: %o', data);
          var type = data.valid;
  
          if (type == 'failed') {
            type = 'Oops!';
          } else {
            type = 'success';
            this.discount_voucher.voucher_value = data.value;
            this.couponcode = this.discount_voucher.voucher_code;
            this.total_value = this.cartservice.total_service - (Number(this.discount_kredit.kredit_value) + Number(data.value));
            /* this.cartservice.total_service-=data.value
            this.cartservice.total_service = this.cartservice.computePrice(this.cartservice.total_service,1); */
          }
          let alert = this.alertCtrl.create({
            title: type,
            subTitle: data.message,
            buttons: ['dismiss']
          });
          alert.present();
          loading.dismiss();
        },
        err => {
          loading.dismiss();
          console.log(err);
        }
      )
    }
  }

  cancelCoupon() {
    let loading = this.loadingCtrl.create({
      content: 'processing\nplease wait...'
    });
    loading.present();
    this.discount_voucher.voucher_code = '';
    this.pushkartService.cancelCoupon().subscribe(
      data => {
        console.log('coupon message: %o', data);
        var type = data.valid;
        if (type == 'failed') {
          type = 'error';
        } else {
          type = 'success';
          this.discount_voucher.voucher_value = 0;
          this.total_value = this.cartservice.total_service - (Number(this.kreditinput) + Number(this.discount_voucher.voucher_value));
        }
        let alert = this.alertCtrl.create({
          title: type,
          subTitle: data,
          buttons: ['dismiss']
        });
        alert.present();
        loading.dismiss();
      },
      err => {
        loading.dismiss();
        console.log(err);
      }
    )
  }

  applykredit() {

    if (this.kreditinput <= this.userkredits) {
      this.discount_kredit.kredit_value = this.kreditinput;
      this.total_value = this.cartservice.total_service - (Number(this.kreditinput) + Number(this.discount_voucher.voucher_value));
      /* this.cartservice.total_service -= this.kreditinput;
      this.cartservice.total_service = this.cartservice.computePrice(this.cartservice.total_service,1); */
      /* let toast = this.toastCtrl.create({message:'kredit applied',duration:2000});
      toast.present(); */
      let alert = this.alertCtrl.create({
        title: 'Hooray!',
        subTitle: 'kredit was applied ',
        buttons: ['Okay']
      });
      alert.present();
    } else {
      let alert = this.alertCtrl.create({
        title: 'oops!',
        subTitle: 'kredit value insufficient ' + this.kreditinput + '\n from current kredits:' + this.userkredits,
        buttons: ['dismiss']
      });
      alert.present();
    }

  }
}
