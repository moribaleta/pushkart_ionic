import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service'; 
import { CartserviceProvider } from '../../providers/cartservice/cartservice'; 
/**
 * Generated class for the SearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  product_list_init; 
  product_list: any; 
  product_filter: any; 
  constructor( 
    public navCtrl: NavController,  
    public navParams: NavParams, 
    public menu: MenuController, 
    public cartservice: CartserviceProvider, 
    public pushkartService: PushkartServiceProvider 
  ) { 
    console.log('cart %o',this.cartservice.cart); 
    this.initSearch(); 
  } 

  ionViewDidLoad() { 
    console.log('ionViewDidLoad SearchPage'); 
    console.log('cart %o',this.cartservice.cart); 
  } 
 
  initSearch() { 
    this.product_list_init = this.pushkartService.product_list; 
    this.product_list = this.product_list_init; 
    console.log('product %O', this.product_list); 
  } 
 
  refreshSearch() { 
    this.product_list = this.product_list_init; 
    console.log('product %O', this.product_list); 
  }
 
  showFilter: Boolean = false; 
  searchFilter(ev: any) { 
    console.log('search'); 
    let val = ev.target.value; 
    this.refreshSearch(); 
    // if the value is an empty string don't filter the product_list 
    if (val && val.trim() != '') { 
      this.product_list = this.product_list.filter((item) => { 
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1); 
      }); 
      this.product_filter = this.product_list; 
      this.product_filter = this.product_filter.slice(0, 30); 
      console.log('product filter %o', this.product_list); 
      this.showFilter = true; 
    } else { 
      this.showFilter = false; 
    } 
  } 
  
  openMenu(evt) {
    if (evt == "main") {
      this.menu.open('category');
    } else {
      this.menu.open('mykart');
    }
  }
  back(){ 
    this.navCtrl.pop(); 
  } 
  increaseQty(value){
    if(Number(value.stock_item)>this.cartservice.cart[value.entity_id].qty){
      this.cartservice.addItemProduct(value);
    }
  }
  decreaseQty(value){
    value.product_id = value.entity_id;
    this.cartservice.decreaseQty(value);
  }
/* 
  increaseQty(value){
    if(Number(value.stock)>this.cartservice.cart[value.entity_id].qty){
      this.cartservice.addItemProduct(value);
    }
  } */

}
