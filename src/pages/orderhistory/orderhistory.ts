import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { OrderDetailsPage } from '../order-details/order-details';

@IonicPage()
@Component({
  selector: 'page-orderhistory',
  templateUrl: 'orderhistory.html',
})
export class OrderhistoryPage {
  Firstname = null;
  order_list: any;
  group_order_list: any;
  order_list_loaded: boolean = false;
  in_progress_count: number = 0;
  kredit_details: any;
  kredit_points: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private pushkartService: PushkartServiceProvider,
    private userService: UserServicesProvider,
    private cartservice: CartserviceProvider
  ) {
    this.Firstname = this.navParams.get('firstname');
    this.getOrderList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderhistoryPage');
    console.log('count prog'+this.Firstname);
    
  }

  getOrderList() {
    this.group_order_list = [];
    this.pushkartService.getOrderlist(this.userService.user).subscribe(
      data => {
        console.log('orderlist: %o', data);
        var array = [];
        for (var key in data) {
          if (data[key].Status == 'Complete' || data[key].Status == 'In Progress' || data[key].Status == 'Cancelled') {
            var Total = data[key].Total;
            data[key].Total = this.cartservice.numberWithCommas(this.cartservice.computePrice(Total, 1));
            array.push(data[key]);
            if(data[key].Status == 'In Progress') {
              this.in_progress_count++;
            }
          }
        }
        this.order_list = array;
        this.group_order_list = this.order_list;
        this.order_list_loaded = true;
      },
      err => {
        console.log(err);
        this.order_list_loaded = true;
      }
    )
  }
  kredit_details_loaded: boolean = false;
  getKredits() {
    this.pushkartService.getKredit(this.userService.user).subscribe(
      data => {
        console.log(data);
        if (!data[0].point_balance) {
          this.kredit_points = 0;
        } else {
          this.kredit_points = data[0].point_balance;
        }
        var data_grid = data[1].transaction[0];
        for (var key in data_grid) {
          data_grid[key].created_time = data_grid[key].created_time.split(' ')[0];
        }
        this.kredit_details = data_grid;
        this.kredit_details = this.kredit_details.reverse();
        this.kredit_details_loaded = true;
      },
      err => {
        console.log(err);
        this.kredit_details_loaded = true;
      }
    )
  }
  openOrderDetails(item) {
    this.navCtrl.push(OrderDetailsPage, { 'order': item, 'kredit': this.kredit_details, 'cart': this.cartservice.cart });
  }
}
