import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { CheckoutPage } from '../checkout/checkout';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions';
/**
 * Generated class for the CheckoutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
/* @IonicPage() */
@Component({
  selector: 'checkout-summary',
  templateUrl: 'checkout_summary.html',
})
export class CheckoutSummaryPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cartservice: CartserviceProvider,
    public pushkartService: PushkartServiceProvider,
    public menu: MenuController,
    public toastCtrl: ToastController,
    public userService: UserServicesProvider
  ) {
    this.menu.enable(false, 'mykart');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');

    this.cartservice.codchecker();
    
  }
  ionViewWillLeave() {
    this.menu.enable(true, 'mykart');
  }

  back() {
    this.navCtrl.pop();
  }

  proceed() {
    if (this.cartservice.total>=500){
      this.cartservice.saveCart();
      this.userService.loginUser();
      this.navCtrl.push(CheckoutPage, { 'couponcode': 'glimsoltest' });
    }else{
      let toast = this.toastCtrl.create({message:'minimum transaction accepted is worth ₱500.00',duration:3000});
      toast.present();
    }
  }

  openTermsAndCondition(){
    this.navCtrl.push(TermsandconditionsPage);
  }

 
  
}
