import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController,LoadingController,MenuController } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';

/**
 * Generated class for the PaymentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
/* @IonicPage() */
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  url: string;
  trustedUrl: any;
  url_message: string;
  payment: string;
  inc: string;
  timer: any;
  loaded: boolean = false;
  loading:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer,
    public pushkartService: PushkartServiceProvider,
    public platform: Platform,
    private cartservice: CartserviceProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public menu: MenuController
  ) {
    this.url = navParams.get('url');
    this.inc = navParams.get('inc');
    this.payment = 'payment'; //https://sandbox-checkout-v2.paymaya.com/checkout?id=37353628-9177-41fa-8d0b-42c75d667fef
    this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    
    this.loading = this.loadingCtrl.create({
      content: 'please wait...'
    });
    this.loading.present();
    this.timer = setInterval(() => {
      this.checkmessage(this.inc);
    }, 5000);


  }
  dismissLoading(){
    this.loading.dismiss();
    this.loaded = true;
  }
  onback() {
    clearInterval(this.timer);
    this.navCtrl.popToRoot();
  }

  checkmessage(inc) {
    console.log('checking');
    this.pushkartService.checkinc(inc).subscribe(
      data => {
        if (data == 'processing') {
          clearInterval(this.timer);
          this.endProcess();
        }
        console.log(data);
      }, err => {

      }
    )
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Cancel Transaction',
      message: 'Do you want to cancel the current transaction?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => { 
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.pushkartService.cancelOrder(this.inc).subscribe(
              data=>{
                this.onback();
              },err=>{
                
              }
            )
            
          }
        }
      ]
    });
    alert.present();
  }

  endProcess() {

    this.cartservice.clearkart();

    let alert = this.alertCtrl.create({
      title: 'Transaction Completed',
      subTitle: 'Transaction process completed',
      buttons: ['home']
    });
    alert.present();
    alert.onDidDismiss(() => {
      this.onback();
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }
  ionViewWillLeave() {
    clearInterval(this.timer);
    this.menu.enable(true, 'mykart');
    console.log('payment page leave ');
  }

}
