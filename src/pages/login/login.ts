import { Component, ViewChild } from '@angular/core';
import { NavController, IonicApp, App, NavParams, ViewController, Platform, MenuController, LoadingController, ToastController, AlertController, Slides } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { MainPage } from '../../pages/main/main';
import { Storage } from '@ionic/storage';
import { IntroPage } from '../intro/intro';
import { SignupPage } from '../signup/signup';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
/* @IonicPage() */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public showPass = false;
  public confshowPass = false;
  public passtype = 'password';
  buttonclass: string = 'not-active';
  email: string = '';
  password: string = '';
  firstname: string = '';
  lastname: string = '';
  conf_password: string = '';
  date: string = '';
  gender: number = 0;
  confirm: boolean = false;
  forgotpassword: boolean = false;
  email_resent: boolean = false;
  login_attemp_count: number = 0;
  @ViewChild(Slides) slides: Slides;
  @ViewChild('loginPassword') input;
  login: boolean = true;
  login_tab_select: string = '2px red solid';
  signup_tab_select: string = '';
  userid: string = '';


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public ionicApp: IonicApp,
    private userService: UserServicesProvider,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public cartservice: CartserviceProvider,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public platform: Platform,
    public app: App,
    public storage: Storage
  ) {
    this.menuCtrl.enable(false, 'category');
    this.menuCtrl.enable(false, 'mykart');
  }

  isNumeric(value) {
    return /^\d+$/.test(value);
  }

  ionViewWillLeave() {
    this.menuCtrl.enable(true, 'category');
    this.menuCtrl.enable(true, 'mykart')
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    
    this.storage.get('user').then((val) => {
      console.log("user login: " + val);
      if (this.isNumeric(val)) {
        this.app.getRootNav().setRoot('HometabsPage');
      }
    });
  }

  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.passtype = 'text';
    } else {
      this.passtype = 'password';
    }
  }

  navSignup() {
    this.navCtrl.push(SignupPage);
  }

  navForgot() {
    this.navCtrl.push('ForgotpassPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  loginApp() {
    if (this.email != '' && this.password != '') {
      var user = { email: this.email, password: this.password };
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.userService.login(user).subscribe(
        data => {
          console.log('login message: %o', data);
          loading.dismiss();
          this.userService.splash = false;
          var message;
          var title;
          var btnmessage;

          if (data == 'Failed') {
            title = 'error';
            message = 'email and password incorrect';
            btnmessage = 'dismiss';
          } else {
            title = 'success!';
            message = 'User login success';
            this.userid = data;
            this.userService.saveUser(data);
            this.userService.saveEmail(user.email);
            this.userService.savePassword(user.password);
            btnmessage = 'continue';
          }

          if (data != 'Failed') {
            this.cartservice.getCartByUser(data);
            this.navCtrl.setRoot('HometabsPage', { 'modal': false, 'getCart': true })
              .then(data => {
                console.log(data);
              }, (error) => {
                console.log(error);
              });
          } else {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: message,
              buttons: [btnmessage]
            });
            alert.present();
          }
          //this.reset();
        },
        err => {
          console.log(err);
          loading.dismiss();
          if (this.login_attemp_count > 2) {
            let alert = this.alertCtrl.create({
              title: 'error',
              subTitle: "can't login from the app try again later",
              buttons: ['dismiss']
            });
            alert.present();
            this.login_attemp_count = 0;
          } else {
            this.login_attemp_count++;
            this.loginApp();
          }

        }
      )
    }
  }

  resetpassword() {
    if (this.email != '' && this.email.length > 10) {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.userService.forgot(this.email).subscribe(
        data => {
          console.log('signup message: %o', data);
          loading.dismiss();
          let alert = this.alertCtrl.create({
            title: 'forgot password',
            subTitle: data,
            buttons: ['dismiss  ']
          });
          alert.present();
        },
        err => {
          loading.dismiss();
          console.log(err);
        }
      )

    } else {
      let toast = this.toastCtrl.create({
        message: 'email not valid',
        duration: 3000,
        position: 'bottom'
      });
      toast.onDidDismiss(() => {
       // this.reset();
      });
      toast.present();
    }
  }

  checkAll() {
    console.log(this.email+'/'+this.password);
    if (this.email != '' && this.password) {
      this.buttonclass = 'active';
    }
    else {
      this.buttonclass = 'not-active';
    }
  }

}
