import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { LoginPage } from '../login/login';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';

@IonicPage()
@Component({
  selector: 'page-signup2',
  templateUrl: 'signup2.html',
})
export class Signup2Page {
  firstname: string = '';
  lastname: string = '';
  date: string = '';
  gender: number = 0;
  buttonclass: string = 'not-active';
  Email = null;
  Mobile = null;
  Password = null;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController, 
    public alertCtrl: AlertController,
    private userService: UserServicesProvider
  ) {
    this.Email = this.navParams.get('Email');
    this.Mobile = this.navParams.get('Mobile');
    this.Password = this.navParams.get('Password');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup2Page');
    console.log(this.Email+'-'+this.Mobile+'-'+this.Password);
  }

  navLogin() {
    this.navCtrl.push(LoginPage);
  }
  navTerms() {
    this.navCtrl.push(TermsandconditionsPage);
  }
  navPrivacy() {
    this.navCtrl.push(PrivacyPolicyPage);
  }

  checkAll() {
    console.log(this.firstname+'/'+this.lastname+'/'+this.date+'/'+this.gender);
    if (
      this.firstname != '' &&
      this.lastname != '' && this.date != '' &&
      this.gender != 0
    ) {
      this.buttonclass = 'active';
    }
    else {
      this.buttonclass = 'not-active';
    }
  }

  signup() {
    var user = {
      email: this.Email,
      mobile: this.Mobile,
      password: this.Password,
      firstname: this.firstname,
      lastname: this.lastname,
      date: this.date,
      gender: this.gender
    };
    console.log('signup %o', user);
    if (
      this.Email != '' && this.firstname != '' &&
      this.lastname != '' && this.date != '' &&
      this.Password != '' && this.Mobile != '' &&
      this.gender != 0
    ) {
      this.userService.signup(user).subscribe(
          data => {
            this.navLogin();
          },
          err => {
            //this.reset();
            console.log(err);
            let alert = this.alertCtrl.create({
              title: 'error',
              subTitle: "can't signup from the app try again later",
              buttons: ['ok']
            });
            alert.present();
          }
        )
    }
    else
    {
      let toast = this.toastCtrl.create({
        message: 'Please Fillup All Fields!',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    }
  }

}
