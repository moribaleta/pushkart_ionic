import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service'; 
import { CartserviceProvider } from '../../providers/cartservice/cartservice'; 
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { LoginPage } from '../login/login';
import { AboutUsPage } from '../about-us/about-us';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';
import { FaqPage } from '../faq/faq';
/**
 * Generated class for the MyAccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {
  userkredits: number = 0;
  params: Object;
  order_list: any;
  dashboard_order_list: any;
  group_order_list: any;
  order_list_loaded: boolean = false;
  order_count_min: number = 0;
  order_count_max: number = 9;
  order_page_max: number = 0;
  in_progress_count: number = 0;
  constructor(
    public navCtrl: NavController,  
    public navParams: NavParams, 
    public menu: MenuController, 
    public cartservice: CartserviceProvider, 
    public userService: UserServicesProvider,
    public loadingCtrl: LoadingController,
    public pushkartService: PushkartServiceProvider 
  ) {
    this.getOrderList();
    this.params = { firstname: this.userService.user_data.firstname };
    
  }
  getOrderList() {
    this.group_order_list = [];
    this.pushkartService.getOrderlist(this.userService.user).subscribe(
      data => {
        console.log('orderlist: %o', data);
        var array = [];
        for (var key in data) {
          if (data[key].Status == 'Complete' || data[key].Status == 'In Progress' || data[key].Status == 'Cancelled') {
            var Total = data[key].Total;
            data[key].Total = this.cartservice.numberWithCommas(this.cartservice.computePrice(Total, 1));
            array.push(data[key]);
            if(data[key].Status == 'In Progress') {
              this.in_progress_count++;
            }
          }
        }
        this.order_list = array;
        this.group_order_list = this.order_list;
        this.order_list_loaded = true;
      },
      err => {
        console.log(err);
        this.order_list_loaded = true;
      }
    )
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAccountPage');
    console.log('cart %o',this.cartservice.cart); 

    this.pushkartService.callKredits().subscribe(
      data => {
        console.log('kredit: ' + data);
        if (data != 'Failed') {
          this.userkredits = Number(data);
        } else {
          this.userkredits = 0;
        }
      }, err => {
        console.log(err);
      }
    )
  }
  openMenu(evt) {
    if (evt == "main") {
      this.menu.open('category');
    } else {
      this.menu.open('mykart');
    }
  }
  openLogout() {
    let loading = this.loadingCtrl.create({
      content: 'logging out...'
    });
    loading.present();
    this.cartservice.clearLogout();
    setTimeout(() => {
      loading.dismiss();
      this.userService.logout();
      this.navCtrl.push(LoginPage);
      this.navCtrl.setRoot(LoginPage).then(data => {
        console.log(data);
      }, (error) => {
        console.log(error);
      })
    }, 3000);
  }

  openTermsandConditions() {
    this.navCtrl.push(TermsandconditionsPage);
  }
  openAboutUs() {
    this.navCtrl.push(AboutUsPage);
  }
  openPrivacyPolicy() {
    this.navCtrl.push(PrivacyPolicyPage);
  }
  openFaq() {
    this.navCtrl.push(FaqPage);
  }

}
