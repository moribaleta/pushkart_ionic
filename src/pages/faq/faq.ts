import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FaqPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
  shownGroup: null;

  general: any;
  ordering: any;
  billing: any;
  delivery: any;
  myaccount: any;
  policies: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.general = [
      {
        title: 'What is Pushkart.ph?',
        desc: 'Pushkart.ph is an online grocery service, developed by young Filipino millenials, that aims to ensure convenient, and satisfaction-guaranteed delivery of groceries right at your doorstep.'
      },
      {
        title: 'What can I buy from Pushkart.ph?',
        desc: 'We’ve got all your grocery needs covered - canned goods, snacks, beverages, or fresh selections. Got a product type that we don’t offer? Let us know!'
      },
      {
        title: 'Why do I need Pushkart.ph?',
        desc: 'Just like you, sometimes things get in the way and we have no time and energy for a quick run to the grocery. And so we thought, what if someone else can buy these things for me, right? A few clicks online and I’m all set. So we created Pushkart.ph to help you out! To give you back some of those previous hours, and avoid the typical 3 hour chore of braving the traffic and carrying heavy bags for your grocery shopping.'
      },
      {
        title: 'Who are our partner stores?',
        desc: 'We are partnered with Fisher Supermarket, to provide the best quality seafood, meat, and other necessities. Supporting other green, organic and local businesses is important to us. We aim to inspire and encourage the community to adopt a healthier and sustainable lifestyle through home-cooked food and more active activities. We are passionate about home-made quality food and we wish to share the joys of our cooking experiences with you.'
      },
      {
        title: 'How does Pushkart’s referral program work?',
        desc: 'With the refer friends or “refer a friend” program, users can get PHP 50.00 for every new user referred. The referring user will share his/her referral code to the new user, wherein the new user will receive PHP 50.00 in his MyKredits immediately upon registering using the said referral code. The referring user will then receive PHP 50.00 in his MyKredits once the new user successfully performs his/her first order through Pushkart.ph. The MyKredits credited can be used for purchases in Pushkart.ph, and are not transferrable or convertible to cash.'
      },
      {
        title: 'Can I create an account for my company (Corporate Account) with Pushkart.ph?',
        desc: 'Corporate Accounts for your company with Pushkart.ph would provide you special offers, discounts, and special arrangements for deliveries. Kindly e-mail us at hello@pushkart.ph if you’d like to create a Corporate Account with us'
      }

    ]
    this.ordering = [
      {
        title: 'How do you ensure the freshness of the products?',
        desc: 'Don’t worry, your orders will be carefully picked by our specialized grocers and will be delivered straight to you via our own fleet of delivery vans or our partner logistics. You just have to wait for our friendly Pushkart.ph grocers to hand you your goods with a smile. We will be transporting goods in temperature-controlled storage boxes to maintain its freshness.'
      },
      {
        title: 'How do I mention my special requests for fresh goods, including fish and seafood? (i.e. fish descaling & deboning)',
        desc: ' Grocery shopping is our job - we’ll absolutely handle your special requests for you. The easiest way is for you to tell us through our “Additional Instructions” input box in #2 of the Checkout Page'
      },
      {
        title: 'Is there a minimum / maximum limit in purchasing?',
        desc: 'There is a minimum order requirement of PHP 499, but there is no maximum order amount! Kindly e-mail us at hello@pushkart.ph if you’d like to create a Corporate Account with us!'
      },
      {
        title: 'What does it mean if an item is “out of stock”?',
        desc: 'Products that are marked “out of stock” are products that are popular or in demand.'
      },
      {
        title: 'Can I suggest a product to add in Pushkart.ph?',
        desc: 'Absolutely, we are always open for any suggestions that you would want to add in our list of grocery items. Just email us at hello@pushkart.ph and our team will make sure your request will be added'
      },
      {
        title: 'How do I make a change to my order?',
        desc: 'No worries - we know it happens. You may message us at hello@pushkart.ph as soon as possible, 4 hours before the confirmed delivery time and we will assist you as best as we can.'
      },
      {
        title: 'What if my purchased item is out of stock and can’t be delivered on-time?',
        desc: 'We will do our best to ensure we provide you all your ordered goods, within the stock inventory of our store partners. If the occasion arrives that one of your paid/ordered products is not available, we will automatically provide you a refund via your MyKredits, which is applicable for your next order.'
      }
    ]
    this.billing = [
      {
        title: 'How are your items priced?',
        desc: 'We believe that online shopping shouldn’t be expensive, which is why we do our best to keep our pricing close to that of our partner stores. To maintain the quality of our service, we charge a fixed service fee, called a “Grocer’s fee”. This might appear as “Shipping fee” in your receipts and confirmations. Grocer’s fee is a convenience fee that we charge for the services provided by our specially screened and trained Grocers in selecting items purchased, and delivering this to your selected address. This lets us helps the families of our grocers as well! The applicable Grocer’s Fee will apply for the ff. order subtotals: \nPHP 1,999.99 or less = PHP 199 Grocer’s fee \nPHP 2,000 to PHP 4,999.99 = PHP 49 Grocer’s fee \nPHP 5,000 and above = Free/Waived Grocer’s fee'
      },
      {
        title: 'Do you have hidden fees?',
        desc: 'Absolutely not. Although much of our industry engages in "upcharging" via hidden fees, we strive to be fully transparent. Our price list is front and center on our homepage, and shortly after you receive your delivery, we will send you an itemized receipt so you know exactly what you were charged. If you ever have any questions on billing, please e-mail us at hello@pushkart.ph and well take care of you.'
      },
      {
        title: 'How can I pay for the items I purchased?',
        desc: 'At this point, we only accept payment via all cards from Visa and Mastercard - credit, debit, and prepaid cards through PayMaya, and over-the-counter via DragonPay. Ordered goods are brought to the buyer’s place and customer will then receive an e-receipt called “order invoice” with our system generated order #. A VAT registered Invoice will be issued upon customer’s request. However, If you don’t have a Visa or Mastercard credit card, you could still purchase from us once you get your virtual Visa card from the *PayMaya App. *PayMaya Philippines Inc. is the first prepaid online payment app that enables the unbanked and the uncarded to pay online without a credit card; PayMaya is the digital financial unit of PLDT and Smart Communications, Inc.'
      },
      {
        title: ' How would I know that my transaction is successful?',
        desc: 'You will receive a Pushkart.ph Order Invoice via the email address you used to register.'
      },
      {
        title: 'Is there any discount for Senior Citizens or Persons with disabilities?',
        desc: 'Discounts for Senior Citizens or Persons with disabilities are not applicable for any on-line transaction.'
      },
      {
        title: 'Can I collect any kind of club points from the supermarkets?',
        desc: 'Currently we do not support the collection of membership points or other savings from stores. Be sure to keep an eye on email and our social media accounts for updates on how we’re expanding to improve this functionality.'
      }

    ];
    this.delivery = [
      {
        title: 'Where is Pushkart.ph available?  ',
        desc: 'Pushkart.ph is currently delivering to Makati, Quezon City, Taguig, Manila, Pasig, San Juan, and Mandaluyong. We will soon launch to other cities, don’t worry :)'
      },
      {
        title: 'What are your delivery hours?',
        desc: 'We deliver from 9:00 AM to 8:00 PM from Mondays to Saturdays. You can view available delivery times on our website at any other two-hour delivery window today and in the next 6 days. If you have a specific time preference, please e-mail hello@pushkart.ph and we will do our best to accommodate.'
      },
      {
        title: 'Can I select a time preference?',
        desc: 'Absolutely - we are happy to work around your schedule.  If you would like us to come at or near a specific time within your chosen two-hour delivery window (e.g. closer to 3:30 PM), please fill this in the delivery instructions and we will do our best to accommodate. If you have a permanent preference, please e-mail our team at hello@pushkart.ph and we will make a note on your account. If you have a request specific to the current day, the simplest way to coordinate with us is by responding to our e-mails or text messages (we will respond ASAP to all messages!).'
      },
      {
        title: 'How do I check the status of my order?',
        desc: 'You will get notifications via SMS and e-mail to confirm the order you placed, and the estimate time of arrival for your order. Kindly respond to the SMS to facilitate the delivery.'
      },
      {
        title: 'Is it possible to have my groceries delivered to an alternate address?',
        desc: 'Yes, as long as the alternate address is within our service area and is communicated before our grocers leave to deliver your goods. Simply e-mail hello@pushkart.ph to let us know what address you would like us to deliver to (plus any instructions), and we’ll take care of the rest.  If the alternate address is a permanent change, you can update your address in the "Profile" section in our "My Account" page.'
      },
      {
        title: 'How do I reschedule my delivery?',
        desc: 'You may message us at hello@pushkart.ph 4 hours before the confirmed delivery time and we will assist you as best as we can. No penalties, just appreciation for giving us the heads up!\nHowever, we know last minute changes occur. So if you need to reschedule your order, we can assist you if your grocer has not left to deliver your goods. Please feel free to contact us at hello@pushkart.ph'
      },
      {
        title: 'How do I cancel my order?',
        desc: 'If you wish to cancel your Order before we have sent you the Order Confirmation before the Order has been dispatched, please contact our Customer Service Team on hello@pushkart.ph No cancellation fees shall be applicable if the cancellation is done 4 hours before the confirmed delivery time. However, once the Order has been dispatched it may not be cancelled.'
      }, {
        title: 'Who does the delivery?',
        desc: 'To ensure the freshness of your goods and fast delivery, our very own fleet of delivery vans or our partner logistics and friendly grocers will do all the work for you so you can have more time for more important things in life, a.k.a. binge watching your favorite TV show.'
      },
      {
        title: 'Are your Grocers good-looking?',
        desc: 'Hey! It’s all about luck, so we cannot promise. Also, it depends whether you have been a very good customer. Kidding aside, what we can always promise is a genuine smile.'
      },
      {
        title: 'What if I am not at home to receive my groceries?',
        desc: 'We get it, sometimes we all enjoy life too much and forget about some things. But if you must know ahead of time that you will be missing our delivery, kindly contact us 4 hours before the confirmed delivery time via e-mail at hello@pushkart.ph so we can reschedule your delivery as soon as possible.\n\nBut if you forget about the delivery, the grocer will leave a notification with the information regarding your groceries and what to do next. And remember that we will have to charge a cancellation/re-delivery fee.'
      },
      {
        title: 'Should I tip my Grocer?',
        desc: 'Please refrain from doing so! :) We have a strict "No Tipping" policy at Pushkart.ph. The act of tipping (or even thinking about how much to tip or whether you tipped enough) is a subtle form of friction that we want to remove from your customer experience!  We compensate our Grocers well and have instructed them not to accept any tips. If you have positive feedback for them, please feel free to e-mail us at hello@pushkart.ph and we will make sure to pass it on to your Grocer.'
      },
    ]
    this.myaccount = [
      {
        title: 'How do I update my contact information?',
        desc:'You can update your contact information in my account > personal information.'
      },{
        title: 'How do I reset my password?',
        desc:' If you’ve forgotten your password, you can reset it in my account > personal information then press change password.'
      },{
        title: 'Where can I view my Order History?',
        desc:'You can view your order history here. If you have any detailed questions that are not answered, feel free to e-mail our team at hello@pushkart.ph.'
      },{
        title: 'What do the Order Status labels mean? ',
        desc:'There are 3 labels for your Order Status: Completed, In-Progress, and Cancelled. The Completed label is tagged once the order was successfully delivered to you. In-Progress means you’re currently paying for your MyKart, a delivery was rescheduled or our team is currently preparing & delivering your order. Lastly, Cancelled occurs in the scenario that you manually have your delivery cancelled.'
      },{
        title: 'Where can I update my grocery order preferences?',
        desc:'We will soon be adding the ability to manage your preferences from your online profile page. Until then, the best way to update your grocery order preferences is by e-mailing our team at hello@pushkart.ph.'
      },{
        title: 'How can I see my previous orders? Can I re-order my previously bought items?',
        desc:' Proceed to Pushkart.ph and log-in using the profile used during your past purchases. Once done, click on my account and select “Order History”. On this page, you will find all previously placed orders with the itemized items. You can even re-order your past purchases!'
      }
    ]
    this.policies = [
      {
        title: 'How do I return items? ',
        desc:'We properly train our grocers to select the best possible goods, for your selections. You are encouraged to check the items upon delivery to ensure the correctness of your items. At this time we don’t accept item returns after the delivery has been completed. If it occurs that we make a mistake in any item you deliver, please make sure to notify our grocer. Our customer support team will then coordinate with you for delivery of the proper items. '
      },
      {
        title: 'What if my special requests for fresh fish is not followed?',
        desc:'We take great pride in the ability of our grocers to be accurate with your instructions for fish. Please check your delivered items before receiving them with our grocer, as we don’t accept item returns after the delivery has been completed. If ever any mistakes were done, please notify our grocer so we can replace your goods.'
      },
      {
        title: 'How can I get everyone at my company to use Pushkart.ph?',
        desc:'We are always excited when our customers want to share Pushkart.ph with their colleagues and employees.  We have several partnerships set up with companies to help them provide employee perks around their grocery needs. If you are interested in exploring this idea, please e-mail us at hello@pushkart.ph and well take care of you.'
      },
      {
        title: 'I want to work for Pushkart.ph….what do I have to do?',
        desc:'We are always excited to add amazing people to our team. If you are interested in working for Pushkart.ph, please check out our job openings here or e-mail hello@pushkart.ph with your resume and tell us why you should work for Pushkart.ph!'
      },
      {
        title: 'How can I partner with Pushkart.ph?',
        desc:'We are excited to hear from you. If you are interested in pushing your kart to the next level, e-mail us at hello@pushkart.ph and we’ll gladly support your cause.'
      },
      {
        title: 'What if some of my paid items are not available?',
        desc:'If your item runs out of stock before we are able to select it from the supermarket for you, we will reach-out to you via the stated mobile number. We can either replace the item or provide you store credit, for use in your next purchase.'
      },
    ]
    /* ,{
      title: '',
      desc:''
    },{
      title: '',
      desc:''
    },{
      title: '',
      desc:''
    }, */

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

}
