import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IntroPage } from '../intro/intro';
import { MainPage } from '../main/main';
import { LoginPage } from '../login/login';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { UserServicesProvider } from '../../providers/user-services/user-services';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  progresspercent: any = 0;
  progresswidth: string = '0%';

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public navParams: NavParams,
    private pushkartService: PushkartServiceProvider,
    public cartservice: CartserviceProvider,
    public userService: UserServicesProvider
  ) {
    
  }

  ionViewDidLoad() {
    var interval = setInterval(() => {
      this.progresspercent++
      if (this.progresspercent == 100) {
        clearInterval(interval);
        this.storage.get('intro-done').then(done => {
          if (!done) {
            this.storage.set('intro-done', true);
            this.navCtrl.setRoot(IntroPage);
          }
          else {
            console.log("user: " + this.userService.user);
            if (this.isNumeric(this.userService.user)) {
              console.log("mainpage");
              this.navCtrl.setRoot('HometabsPage');
              this.userService.loginUser();
              this.cartservice.initialize();
              this.cartservice.user = this.userService.user;
            }
            else {
              console.log("loginpage");
              this.navCtrl.setRoot(LoginPage);
            }
          }
        });
      }
      this.progresswidth = this.progresspercent + '%';
      console.log('percent'+this.progresspercent);
    }, 20);
  }

  isNumeric(value) {
    return /^\d+$/.test(value);
  }
}
