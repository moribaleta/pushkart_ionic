import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaypalpaymentPage } from './paypalpayment';

@NgModule({
  declarations: [
    PaypalpaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(PaypalpaymentPage),
  ],
  exports: [
    PaypalpaymentPage
  ]
})
export class PaypalpaymentPageModule {}
