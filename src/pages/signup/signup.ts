import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  email: string = '';
  mobile: string = '';
  password: string = '';
  conf_password: string = '';
  buttonclass: string = 'not-active';
  public passtype = 'password';
  public confpasstype = 'password';
  public showPass = false;
  public confshowPass = false;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  navLogin() {
    this.navCtrl.push(LoginPage);
  }

  navNext() {
    if (
      this.email != '' && this.mobile != '' &&
      this.password != '' && this.conf_password != ''
    ) {
      if (this.password == this.conf_password) {
        this.navCtrl.push('Signup2Page', {Email: this.email, Mobile: this.mobile, Password: this.password});
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Password doesnt match',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
      }
    }
    else
    {
      let toast = this.toastCtrl.create({
        message: 'Please Fillup All Fields!',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.passtype = 'text';
    } else {
      this.passtype = 'password';
    }
  }
  confshowPassword() {
    this.confshowPass = !this.confshowPass;
 
    if(this.confshowPass){
      this.confpasstype = 'text';
    } else {
      this.confpasstype = 'password';
    }
  }

  checkAll() {
    console.log(this.email+'/'+this.mobile+'/'+this.password+'/'+this.conf_password);
    if (
      this.email != '' && this.mobile != '' &&
      this.password != '' && this.conf_password != ''
    ) {
      this.buttonclass = 'active';
    }
    else {
      this.buttonclass = 'not-active';
    }
  }
}
