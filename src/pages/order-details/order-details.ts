import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { CheckoutSummaryPage } from '../checkout_summary/checkout_summary';
/**
 * Generated class for the OrderDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
// @IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
})
export class OrderDetailsPage {

  order: any;
  order_details: any;
  order_details_loaded: boolean = false;
  grandtotal: any;
  kredit_detail: any;
  /*  product_details: any; */
  product_group: Array<any>;
  earnpoints: any;
  cartlist: any;
  has_kredit: Boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pushkartService: PushkartServiceProvider,
    public cartservice: CartserviceProvider,
    public alertCtrl: AlertController
  ) {
    this.cartlist = navParams.get('cart');
    this.order = navParams.get('order');
    var kredit_list = navParams.get('kredit');
    console.log('cartservice cart %o', this.cartservice.cart);
    for (var key in kredit_list) {
      if (this.order.Order_id == kredit_list[key].order_increment_id) {
        this.kredit_detail = kredit_list[key];
        console.log('kredit detail %o', this.kredit_detail);
        this.kredit_detail.discount = Number(this.kredit_detail.discount);
        this.has_kredit = true;
        break;
      }
    }


    this.getOrderDetails(this.order.Order_id);
  }

  getOrderDetails(id) {
    this.pushkartService.getOrderDetails(id).subscribe(
      data => {
        this.order_details_loaded = true;
        console.log(data);
        this.order_details = data;
        console.log(data[3].shipping_address);
        this.parseItems(data[7].items);
        this.order_details = {
          "status": data[0].status,
          "payment_method": data[1].payment_method,
          "shipping_method": data[2].shipping_method,
          "shipping_address": data[3].shipping_address[0].split(','),
          "billing_address": data[4].billing_address[0].split(','),
          "slot_booking_information": data[5].slot_booking_information[0].split(','),
          "order_comment": data[6].order_comment,
          "items": data[7].items,
          "date_ordered": data[8].date_ordered,
          "coupon_discount": data[9]['coupon discount'],
          "subtotal": data[10].subtotal,
          "shipping_fee": data[11].shipping_fee,
          "tax": data[12].tax,
          "grandtotal": data[13].grandtotal,
          "oid": data[14].oid
        };
        /* console.log('products_details %o', this.product_details); */
        /* this.product_group = this.product_details;   */
        console.log('order_details: %o', this.order_details);
      },
      err => {
        console.log(err);
      }
    )
  }

  printOrder() {
    window.open(this.cartservice.url+'sales/order/print/order_id/'+this.order_details.oid, '_system', 'location=no');
  }

  parseItems(products) {
    this.grandtotal = 0;
    var product_details = [];
    for (var i in products) {
      var detail = products[i].split(',');
      var qty = detail[4];
      qty = qty.substr(0, qty.length - 2);
      qty = Number(qty);
      var small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/placeholder/default/image-not-available_1.png';
      try {
        small_image = this.cartlist[detail[0]].small_image;
      } catch (err) {

      }
      //console.log('product_list: %o',this.cartlist); 
      var item = {
        product_id: detail[0],
        name: detail[1],
        sku: detail[2],
        price: detail[3],
        qty: qty,
        total: Number(detail[3]) * qty,
        small_image: small_image
      };
      this.grandtotal += Number(detail[3]) * qty;
      if (this.grandtotal > 1000) {
        this.earnpoints = this.grandtotal / 1000;
        this.earnpoints = Math.trunc(this.earnpoints);
      }
      product_details.push(item);
    }
    this.product_group = product_details;
    console.log('products_details %o', this.product_group);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDetailsPage');
  }

  reorderItems() {
    for (var key in this.product_group) {
      console.log('adding item %o', this.cartlist[this.product_group[key].product_id]);
      for (var i = 0; i < this.product_group[key].qty; i++) {
        this.cartservice.addItemProduct(this.cartlist[this.product_group[key].product_id]);
      }
      //this.cartservice.cart[this.product_group[key].product_id].qty = this.product_group[key].qty;

    }
    this.navCtrl.push(CheckoutSummaryPage);
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm reorder',
      message: 'Do you want to reorder this items',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            /*  console.log('Buy clicked'); */
            this.reorderItems();
          }
        }
      ]
    });
    alert.present();
  }

}
