import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
/**
 * Generated class for the PaymentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {
  constructor(
    public navCtrl: NavController,
  ) {
   
  }

  onback() {
    this.navCtrl.pop();
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

}
