import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
/**
 * Generated class for the ProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
/* @IonicPage() */
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  value: any;
  qty: any;
  qty_temp: any;
  imgloaded: boolean = false;
  images: string = 'hidden';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cartservice: CartserviceProvider,
    public pushkartService: PushkartServiceProvider,
    public toastCtrl: ToastController
  ) {

    this.value = this.navParams.get('product');
    this.value['product_id'] = this.value.entity_id;
    if (cartservice.cart[this.value.entity_id].qty != undefined) {
      this.qty = cartservice.cart[this.value.entity_id].qty;
    } else {
      this.qty = 0;
    }
    this.value.weight = cartservice.computePrice(this.value.weight, 1);
    this.value.price = cartservice.computePrice(this.value.price, 1);
    this.qty_temp = this.qty;
    console.log('product %o', this.value);
    console.log('qty : ' + this.qty);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

  close() {
    this.navCtrl.pop();
  }

  eventHandler(event) {
    console.log('user input qty: '+this.qty_temp+' vs stock: '+this.value.stock);
    if (event == 13 || event == 10) {
      if (this.qty_temp == '') {
        this.qty_temp = 0;
      }else if(Number(this.qty_temp) > Number(this.value.stock)){
        console.log('max stock reached');
        this.qty_temp = this.value.stock;
        this.qty_temp = Math.round(this.qty_temp);
        let toast = this.toastCtrl.create({message:'exceeds maximum quantity available',duration: 2000,position:'center'});
        toast.present();
      }
      this.qty = this.qty_temp;
      console.log(this.qty);
      //this.cartservice.cart[this.value.entity_id].qty = this.qty;
      this.cartservice.updateQty(this.value, this.qty);
    }

  }
  updateQty(value) {
    setTimeout(() => {
      if (this.qty_temp == '') {
        this.qty_temp = 0;
      }
      this.qty = this.qty_temp;
      this.cartservice.updateQty(this.value, this.qty);
    }, 5000);
  }

  addItem() {
    this.qty++;
    this.qty_temp = this.qty;
    console.log('qty: ' + this.qty_temp);
    this.cartservice.addItemProduct(this.value);
  }

  increaseQty() {
    if (this.qty<this.value.stock){
      this.qty++;
      this.qty_temp = this.qty;
      console.log('qty: ' + this.qty_temp);
      this.cartservice.updateQty(this.value, this.qty);
    }
  }
  decreaseQty() {
    this.qty--;
    this.qty_temp = this.qty;
    console.log('qty: ' + this.qty_temp);
    this.cartservice.updateQty(this.value, this.qty);
  }
  productload() {
    this.imgloaded = true;
    this.images = 'visible';
    console.log('loaded img');
  }
}
