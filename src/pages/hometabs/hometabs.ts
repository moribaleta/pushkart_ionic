import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { MainPage } from '../main/main';
import { MyaccountPage } from '../myaccount/myaccount';

@Component({
  selector: 'page-hometabs',
  templateUrl: 'hometabs.html'
})
@IonicPage()
export class HometabsPage {

  homeRoot = MainPage
  searchRoot = 'SearchPage'
  needHelpRoot = 'NeedHelpPage'
  myListRoot = 'MyListPage'
  myAccountRoot = 'MyAccountPage'


  constructor(public navCtrl: NavController) {}

}
