import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccesspagePage } from './successpage';

@NgModule({
  declarations: [
    SuccesspagePage,
  ],
  imports: [
    IonicPageModule.forChild(SuccesspagePage),
  ],
  exports: [
    SuccesspagePage
  ]
})
export class SuccesspagePageModule {}
