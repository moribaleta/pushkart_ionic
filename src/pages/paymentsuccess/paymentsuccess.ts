import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OrderDetailsPage } from '../order-details/order-details';

@IonicPage()
@Component({
  selector: 'page-paymentsuccess',
  templateUrl: 'paymentsuccess.html',
})
export class PaymentsuccessPage {
  OrderId = null;
  user_data: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userService: UserServicesProvider,
    private pushkartService: PushkartServiceProvider,
    public storage: Storage
  ) {
    this.OrderId = this.navParams.get('orderId');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentsuccessPage');
  }

  goHome() {
    //this.navCtrl.push(MainPage);
    this.navCtrl.popToRoot();
  }
  
  /* orderdetails() {
    console.log('order details %o',this.pushkartService.getOrderDetails(this.OrderId));
    this.navCtrl.push(OrderDetailsPage, { 'order': {Order_id : this.OrderId}});
  } */

}
