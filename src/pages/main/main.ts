import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, Content, Scroll, ToastController, NavParams, Platform, AlertController, App, Slides } from 'ionic-angular';
import { CategoriesPage } from '../categories/categories';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { ProductPage } from '../product/product';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the MainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  home_category_header: any;
  home_category: {};
  itemloaded: Boolean = false;
  opensearch: Boolean = false;
  home_catid: [12, 8, 11, 71];
  product_list: any;
  product_filter: any;
  floater_element: any;
  view_categories: boolean = false;
  category_list: string = 'floating-list hidden';
  timeInterval: any;

  @ViewChild(Content) content: Content;
  @ViewChild('banner') banner;
  @ViewChild('floater') floater;
  @ViewChild(Slides) slides: Slides;
  
  constructor(public navCtrl: NavController,
    private Storage: Storage,
    private pushkartService: PushkartServiceProvider,
    public cartservice: CartserviceProvider,
    public navParams: NavParams,
    public menu: MenuController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public userService: UserServicesProvider,
    public alertCtrl: AlertController,
    public app: App
  ) {
    this.home_category_header = ['Snacks', 'Beverage', 'Dairy and Eggs', 'Canned Goods'];
    this.getMainProduct();
    
    this.getUserInfo();
    platform.ready().then(() => {
      var lastTimeBackPress = 0;
      var timePeriodToExit = 2000;
      platform.registerBackButtonAction(() => {
        // get current active page
        let view = this.navCtrl.getActive();

        if (this.view_categories) {
          //this.openCategories();
        } else {
          if (view.component.name == "MainPage" || view.component.name == "LoginPage") {
            //Double check to exit app                  
            if (this.view_categories) {
              //this.openCategories();
            } else {
              if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                this.cartservice.dataDump();
                setTimeout(() => {
                  this.platform.exitApp();
                }, 2000);
                //Exit from app
                window.close();
              } else {
                let toast = this.toastCtrl.create({
                  message: 'Press back again to exit App?',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
                lastTimeBackPress = new Date().getTime();
              }
            }
          } else {
            // go to previous page
            this.navCtrl.pop({});
          }
        }
      });
    });
  }

  ionViewDidLoad() {    
    console.log('ionViewDidLoad HomePage');
  }

  /* ngAfterViewInit() {
    this.slides.loop = true;
  } */

  getUserInfo() {
    var user_info = {
      'firstname': '',
      'lastname': ''
    };
    setTimeout(
      () => {
        this.pushkartService.getInfo(this.userService.email).subscribe(
          data => {
            console.log("user data %o", data);
            var gender = '';
            try {
              gender = data[2].gender;
            } catch (err) {
              gender = '';
            }
            user_info = {
              'firstname': data[0].firstname,
              'lastname': data[1].lastname
            };
            console.log("user data %o", user_info);
            this.userService.saveUserData(user_info);
            this.userService.user_data = user_info;
          }, err => {
          }
        )
      }, 1000
    )
  }

  getMainProduct() {
    this.pushkartService.getItems().subscribe(
      data => {
        console.log('product_list %o', data);
        for (var key in data) {
          data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
          data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
          data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);
        }
        this.pushkartService.product_list = data;
        this.cartservice.cart_product_loaded = true;
        this.pushdata();
        this.getCart();
      },
      err => {
        console.log(err);
      }
    )
  }

  pushdata() {
    this.cartservice.cart = {};
    for (var key in this.pushkartService.product_list) {
      this.pushkartService.product_list[key].qty = 0;
      this.cartservice.cart[this.pushkartService.product_list[key].entity_id] = this.pushkartService.product_list[key];
    }
    console.log('cart %o', this.cartservice.cart);
  }

  getproduct_list() {
    this.count = 0;
    var home_product_list = {};
    this.pushkartService.getProductbyCategories(35, 6).subscribe(
      data => {
        for (var key in data) {
          data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
          data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
          data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);
          try {
            data[key].stock = this.cartservice.cart[data[key].entity_id].stock_item;
          } catch (err) {
            console.log(err);
          }
        }
        home_product_list['Snacks'] = data;
        this.home_category = home_product_list;
        this.itemfinish();
      },
      err => {
        this.getproduct_list();
      }
    )
    this.pushkartService.getProductbyCategories(150, 6).subscribe(
      data => {
        for (var key in data) {
          data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
          data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
          data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);
          console.log("product item: " + data[key].name + " - " + data[key].price);
          try {
            data[key].stock = this.cartservice.cart[data[key].entity_id].stock_item;
          } catch (err) {
            console.log(err);
          }
        }
        home_product_list['Beverage'] = data;
        this.home_category = home_product_list;
        this.itemfinish();
      },
      err => {
        this.getproduct_list();
        console.log(err);
      }
    )
    this.pushkartService.getProductbyCategories(54, 6).subscribe(
      data => {
        for (var key in data) {
          data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
          data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
          data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);
          console.log("product item: " + data[key].name + " - " + data[key].price);
          try {
            data[key].stock = this.cartservice.cart[data[key].entity_id].stock_item;
          } catch (err) {
            console.log(err);
          }
        }
        home_product_list['Dairy and Eggs'] = data;
        this.home_category = home_product_list;
        this.itemfinish();
      },
      err => {
        this.getproduct_list();
        console.log(err);
      }
    )
    this.pushkartService.getProductbyCategories(79, 6).subscribe(
      data => {
        for (var key in data) {
          data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
          data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
          data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);
          console.log("product item: " + data[key].name + " - " + data[key].price);
          try {
            data[key].stock = this.cartservice.cart[data[key].entity_id].stock_item;
          } catch (err) {
            console.log(err);
          }
        }
        home_product_list['Canned Goods'] = data;
        this.home_category = home_product_list;
        this.itemfinish();
      },
      err => {
        this.getproduct_list();
        console.log(err);
      }
    )
  }

  count: number = 0;
  itemfinish() {
    this.count++;
    if (this.count >= 1) {
      this.cartservice.cart_loaded = true;
      setTimeout(() => {
        this.itemloaded = true;
        this.userService.splash = false;
      }, 500);
    }
  }

  openPage(page) {
    this.navCtrl.push(CategoriesPage, { 'category': page });
    /* if (this.view_categories) {
      setTimeout(() => {
        //this.openCategories();
      }, 1000);
    } */
  }
  moveslide(slide) {
    this.slides.slideTo(slide, 500);
  }

  openMenu(evt) {
    if (evt == "main") {
      this.menu.open('category');
    } else {
      this.menu.open('mykart');
    }
    this.menu.toggle();
    if (this.view_categories) {
      //this.openCategories();
    }
  }

  toggleSearch() {
    if (this.view_categories) {
      //this.openCategories();
    }
    this.navCtrl.push('SearchPage');
  }

  gototop() {
    this.content.scrollToTop();
  }

  getCart() {
    this.cartservice.cart_display_item = [];
    this.cartservice.getCartByUserData(this.userService.user).subscribe(
      data => {

        this.cartservice.cart_count = 0;
        for (var key in data) {
          this.cartservice.cart_count += data[key].qty;
          var item = data[key];
          try {
            item.small_image = this.cartservice.cart[item.product_id].small_image;
            item.thumbnail = this.cartservice.cart[item.product_id].thumbnail;
          } catch (err) {
            item.small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/placeholder/default/image-not-available_1.png';
            item.thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/placeholder/default/image-not-available_1.png';
          }
          if (!this.existincart(item)) {
            this.cartservice.cart_display_item.push(item);
          }

        }
        for (var key in this.cartservice.cart_display_item) {
          try {
            var item = this.cartservice.cart_display_item[key];
            this.cartservice.cart[item.product_id].qty = item.qty;
          } catch (err) {
            this.cartservice.cart_display_item.splice(key, 1);

          }
        }
        this.cartservice.getTotal();
        this.getproduct_list();
      },
      err => {
        this.getproduct_list();
      }
    )
  }

  existincart(item) {
    for (var key in this.cartservice.cart_display_item) {
      if (this.cartservice.cart_display_item[key].product_id == item.product_id) {
        return true;
      }
    }
    return false;
  }

  openProductModal(product) {
    this.navCtrl.push(ProductPage, { 'product': product });
  }

  increaseQty(value) {
    if (Number(value.stock) > this.cartservice.cart[value.entity_id].qty) {
      this.cartservice.addItemProduct(value);
    }
  }

  decreaseQty(value) {
    console.log('value: %o', value);
    try {
      this.cartservice.decreaseQty(value);
    } catch (err) {
      console.log(err);
    }
  }

}
