import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-services/user-services';
/**
 * Generated class for the ModalAddressPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
/* @IonicPage() */
@Component({
  selector: 'page-modal-address',
  templateUrl: 'modal-address.html',
})
export class ModalAddressPage {
  user_details: any;
  cities: Array<any>;
  edit: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alert: AlertController,
    public userService: UserServicesProvider,
    public loading: LoadingController,
    public viewCtrl: ViewController
  ) {
    if (navParams.get('edit')) {
      this.user_details = navParams.get('address');
      this.edit = true;
    } else {
      this.user_details = {
        firstname: '',
        lastname: '',
        village: '',
        street: '',
        postcode: '',
        city: '',
        telephone: '',
        mobile: '',
      }
    }
    this.cities = [
      'Caloocan'
      , 'Las Pinas'
      , 'Makati'
      , 'Malabon'
      , 'Mandaluyong'
      , 'Manila'
      , 'Marikina'
      , 'Muntinlupa'
      , 'Navotas'
      , 'Paranaque'
      , 'Pasay'
      , 'Pasig'
      , 'Pateros'
      , 'Quezon City'
      , 'San Juan'
      , 'Taguig'
      , 'Valenzuela'
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAddressPage');
  }

  saveAddress() {
    /* firstname: '',
      lastname: '',
      village: '',
      street: '',
      postcode: '',
      city: '',
      telephone: '',
      mobile: '', */

    let loader = this.loading.create(
      {
        content: 'please wait'
      }
    );
    loader.present();
    if (
      (this.user_details.firstname != '')
      &&
      (this.user_details.lastname != '')
      &&
      (this.user_details.village != '')
      &&
      (this.user_details.street != '')
      &&
      (this.user_details.postcode != '')
      &&
      (this.user_details.city != '')
      &&
      (this.user_details.telephone != '')
      &&
      (this.user_details.mobile != '')
    ) {
      if (!this.edit) {
        this.userService.createAddress(this.user_details).subscribe(
          data => {
            console.log(data);
            let alert = this.alert.create({
              title: 'Success',
              subTitle: 'user address added',
              buttons: ['Dismiss']
            });
            alert.present();
            loader.dismiss();
            this.closeSuccess();
          }, err => {
            console.log(err);
            loader.dismiss();
          }
        )
      } else {
        this.userService.editAddress(this.user_details).subscribe(
          data => {
            console.log(data);
            let alert = this.alert.create({
              title: 'Success',
              subTitle: 'user address edited',
              buttons: ['continue']
            });
            alert.present();
            loader.dismiss();
            this.closeSuccess();
          }, err => {
            console.log(err);
            loader.dismiss();
          }
        )
      }
    } else {
      loader.dismiss();
      let alert = this.alert.create({
        title: 'Invalid Input',
        subTitle: 'fill up all the following information needed',
        buttons: ['Dismiss']
      });
      alert.present();
    }

  }

  closeSuccess() {
    this.viewCtrl.dismiss({ user_details:this.user_details });
  }


}
