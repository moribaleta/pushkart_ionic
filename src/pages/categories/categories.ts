import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { NavController, NavParams, MenuController, Content, AlertController, Slides } from 'ionic-angular';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
import { ProductPage } from '../product/product';
/**
 * Generated class for the CategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 *//* 
@IonicPage() */
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html'
})
export class CategoriesPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild('floater') floater;
  @ViewChild('tablistcontent') tablist: Content;
  @ViewChildren('tabs') tabs: QueryList<any>;
  @ViewChild('tablist') tablist_parent;

  category: any;
  category_children: any;
  items: any;
  items_temp: any;
  cart_items: any;
  itemlimit: any;
  itemloaded: Boolean = false;
  itemsize: any;
  sample: any;
  opensearch: Boolean = false;
  product_list: any;
  product_filter: any;
  floater_element: any;
  view_categories: Boolean = false;
  category_list: string = 'floating-list hidden';
  max_offset: number = 0;
  firstIndex: boolean = true;

  @ViewChild(Content) content: Content;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private pushkartService: PushkartServiceProvider,
    public cartservice: CartserviceProvider,
    private menu: MenuController,
    private alertCtrl: AlertController) {
    this.itemlimit = 12;

    this.category = navParams.get('category');
    console.log('category %o', this.category);
    this.getItems(this.category.entity_id);

  }

  /* ngAfterViewInit() {
    this.floater.nativeElement.className = 'floating-category';
  }
 */


  openPage(page) {
    if (page.name != this.category.name) {
      this.navCtrl.push(CategoriesPage, { 'category': page });
    }
    if (this.view_categories) {
      setTimeout(() => {
        this.openCategories();
      }, 1000);
    }

  }

  openIndex(i) {
    this.slides.slideTo(i, 500);
    for (var x in this.category_children) {
      this.category_children[x].class = '';
    }
    this.category_children[i].class = 'active-block';
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    if (currentIndex >= this.category_children.length - 1) {
      currentIndex = this.category_children.length - 1;
    }

    for (var x in this.category_children) {
      this.category_children[x].class = '';
      this.category_children[x].load = false;
    }
    this.category_children[currentIndex].class = 'active-block';

    this.category_children[currentIndex].load = true;
    var i = 0;
    var offset = 0;
    this.tabs.forEach(div => {
      if (i == currentIndex) {
        offset = div.nativeElement.offsetLeft;
      }
      i++;
    });
    this.tablist.scrollTo(offset, 0, 500);
  }


  getItems(id) {
    this.cart_items = {};
    this.category_children = [];
    this.pushkartService.getProductbyCategories(id, '').subscribe(
      data => {

        if (data.length < this.pushkartService.product_list.length && data.length != 0) {
          for (var key in data) {
            data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
            data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
            data[key].stock = this.cartservice.cart[data[key].entity_id].stock_item;
            data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);

            data[key].name = data[key].name.toLowerCase();
          }
          this.items = data;
          console.log('items ' + id + ' %o', this.items);
          this.itemsize = data.length;
          this.pushdata(data);
          var item = { name: this.category.name, products: this.items, class: 'active-block', load: true };
          this.firstIndex = false;
          this.category_children.push(item);
        }
        this.itemloaded = true;
        this.getSubcategoryItems();

      },
      err => {
        console.log(err);
        this.itemloaded = true;
      }
    )
  }

  getSubcategoryItems() {

    console.log(this.category_children);
    for (var key in this.category.children_nodes) {
      console.log('child: %o', this.category.children_nodes[key])
      this.getSubcatItemData(this.category.children_nodes[key]);
    }
  }


  getSubcatItemData(item) {
    var name = item.name;
    this.pushkartService.getProductbyCategories(item.entity_id, '').subscribe(
      data => {
        if (data.length < this.pushkartService.product_list.length) {
          for (var key in data) {
            data[key].small_image = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/small_image/195x195/9df78eab33525d08d6e5fb8d27136e95/' + data[key].small_image;
            data[key].thumbnail = 'https://cdn.pushkart.ph/media/catalog/product/cache/1/image/600x/3cb5e935a2bfbd302b3cfd159df69857/' + data[key].thumbnail;
            data[key].price = Number(data[key].price) + (Number(data[key].price) * this.pushkartService.tax_value);
            data[key].name = data[key].name.toLowerCase();
          }
          var products = data;
          var slot = { name: name, products: products, class: '', load: false }
          if (this.firstIndex) {
            this.firstIndex = false;
            slot.class = 'active-block';
          }

          if (products.length > 0) {
            this.category_children.push(slot);
            console.log('subcat: ' + slot);
          }
        } else {
          return null;
        }
      },
      err => {
        console.log(err);
        return null;
      }
    )
  }

  openCategories() {
    this.view_categories = !this.view_categories;
    if (this.view_categories) {
      this.category_list = 'floating-list';
      this.floater.nativeElement.className = 'floating-category expand';
    } else {
      this.category_list = 'floating-list hidden';
      this.floater.nativeElement.className = 'floating-category';
    }

  }

  pushdata(data) {
    for (var key in data) {
      if (this.cartservice.cart[data[key].entity_id] == null) {
        data[key].qty = 0;
        this.cartservice.cart[data[key].entity_id] = data[key];
      }
    }
    this.cart_items = this.cartservice.cart;
    this.itemloaded = true;
  }

  openMenu(evt) {
    if (evt == "main") {
      this.menu.open('category');
    } else {
      this.menu.open('mykart');
    }
    if (this.view_categories) {
      this.openCategories();
    }
  }

  goHome() {
    //this.navCtrl.push(MainPage);
    this.navCtrl.popToRoot();
  }

  toggleSearch() {
    this.navCtrl.push('SearchPage');
    if (this.view_categories) {
      this.openCategories();
    }
  }
  gototop() {
    this.content.scrollToTop();
    if (this.view_categories) {
      this.openCategories();
    }
  }

  openProductModal(product) {
    this.navCtrl.push(ProductPage, { 'product': product });
  }

  increaseQty(value) {
    if (Number(value.stock) > this.cartservice.cart[value.entity_id].qty) {
      this.cartservice.addItemProduct(value);
    }
  }
}
