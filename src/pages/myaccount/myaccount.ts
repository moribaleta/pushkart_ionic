import { Component, ViewChild, } from '@angular/core';
import { Content, NavController, NavParams, MenuController, Slides, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-services/user-services';
import { PushkartServiceProvider } from '../../providers/pushkart-service/pushkart-service';
import { ModalAddressPage } from '../modal-address/modal-address';
import { OrderDetailsPage } from '../order-details/order-details';
import { CartserviceProvider } from '../../providers/cartservice/cartservice';
/**
 * Generated class for the MyaccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
// @IonicPage()
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild('scrollgrid') scrollgrid: Content;

  @ViewChild('tablist') myTablist;
  @ViewChild('tab1') myTab1;
  @ViewChild('tab2') myTab2;
  @ViewChild('tab3') myTab3;
  @ViewChild('tab4') myTab4;
  @ViewChild('tab5') myTab5;
  @ViewChild('tab6') myTab6;

  user: any;
  loaded_address: boolean = false;
  user_details: any;
  user_details_exist: boolean = false;
  user_info: any;
  user_info_loaded: boolean = false;
  change_pass: boolean = false;
  group_address: any;
  order_list: any;
  dashboard_order_list: any;
  group_order_list: any;
  order_list_loaded: boolean = false;
  page_count: number = 1;
  order_count_min: number = 0;
  order_count_max: number = 9;
  order_page_max: number = 0;
  kredit_details: any;
  kredit_points: any;
  start_page: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private userService: UserServicesProvider,
    private modal: ModalController,
    private alert: AlertController,
    private pushkartService: PushkartServiceProvider,
    private loadingCtrl: LoadingController,
    private cartservice: CartserviceProvider
  ) {
    this.start_page = Number(navParams.get('index'));
    console.log(this.start_page);
    this.user_info = {
      'firstname': '',
      'lastname': '',
      'email': '',
      'mobile': '',
      'gender': '',
      'password': '',
      'change_pass': false,
      'new_password': '',
      'conf_password': '',
    };
    this.getAddresses();
    this.getOrderList();
    this.getUserInfo();
    this.getKredits();
    this.menu.enable(false, 'mykart');
    this.menu.enable(false, 'category');
    console.log('cartservice cart %o', this.cartservice.cart);
  }
  ionViewWillLeave() {
    this.menu.enable(true, 'mykart');
    this.menu.enable(true, 'category');
  }

  kredit_details_loaded: boolean = false;
  getKredits() {
    this.pushkartService.getKredit(this.userService.user).subscribe(
      data => {
        console.log(data);
        if (!data[0].point_balance) {
          this.kredit_points = 0;
        } else {
          this.kredit_points = data[0].point_balance;
        }
        var data_grid = data[1].transaction[0];
        for (var key in data_grid) {
          data_grid[key].created_time = data_grid[key].created_time.split(' ')[0];
        }
        this.kredit_details = data_grid;
        this.kredit_details = this.kredit_details.reverse();
        this.kredit_details_loaded = true;
      },
      err => {
        console.log(err);
        this.kredit_details_loaded = true;
      }
    )
  }
  group_address_list: any;
  getAddresses() {
    this.userService.getUserAddressOnline().subscribe(
      data => {
        this.loaded_address = true;
        if (data.length > 0) {
          this.user_details_exist = true;
          this.loaded_address = true;
          this.group_address = data;
          this.user_details = data[0];
          this.group_address_list = this.group_address.splice(1, this.group_address.length - 1);
        }
        console.log('address : %o', data[0]);
      },
      err => {
        console.log(err);
        this.user_details_exist = true;
        this.loaded_address = true;
      }
    )
  }

  getOrderList() {
    this.group_order_list = [];
    this.pushkartService.getOrderlist(this.userService.user).subscribe(
      data => {
        console.log('orderlist: %o', data);
        var array = [];
        for (var key in data) {
          if (data[key].Status == 'Complete' || data[key].Status == 'In Progress') {
            var Total = data[key].Total;
            data[key].Total = this.cartservice.numberWithCommas(this.cartservice.computePrice(Total, 1));
            array.push(data[key]);
          }
        }

        
        this.order_list = array;
        this.dashboard_order_list = this.order_list.slice(0, 9);
        this.group_order_list = this.order_list.slice(0, 9);
        this.order_page_max = Math.ceil(data.length / 10);
        console.log(this.order_page_max);
        this.order_list_loaded = true;
      },
      err => {
        console.log(err);
        this.order_list_loaded = true;
      }
    )
  }

  getUserInfo() {
    this.user_info = {
      'firstname': '',
      'lastname': '',
      'email': '',
      'mobile': '',
      'gender': '',
      'password': '',
      'change_pass': false,
      'new_password': '',
      'conf_password': '',
    };
    this.pushkartService.getInfo(this.userService.email).subscribe(
      data => {
        var gender = '';
        try {
          gender = data[2].gender;
        } catch (err) {
          gender = '';
        }
        this.user_info = {
          'firstname': data[0].firstname,
          'lastname': data[1].lastname,
          'email': this.userService.email,
          'mobile': '',
          'password': '',
          'gender': gender,
          'change_pass': false,
          'new_password': '',
          'conf_password': '',
        };
        console.log(this.user_info);
      }, err => {
        console.log(err);

      }
    )
  }

  orderpageForward() {
    console.log('move');
    if (this.order_list.length > 10) {
      if (this.page_count < this.order_page_max) {
        this.group_order_list = this.order_list.slice(this.order_count_max, this.order_count_max + 9);
        this.order_count_min = this.order_count_max;
        this.order_count_max += 9;
        this.page_count++;
      }
    }
  }
  orderpageBackward() {
    console.log('move');
    if (this.page_count > 1) {
      this.group_order_list = this.order_list.slice(this.order_count_min - 9, this.order_count_min);
      this.order_count_max = this.order_count_min;
      this.order_count_min -= 9;
      this.page_count--;
    }
  }

  openOrderDetails(item) {
    this.navCtrl.push(OrderDetailsPage, { 'order': item, 'kredit': this.kredit_details, 'cart': this.cartservice.cart });
  }



  saveUserInfo() {
    if (!this.user_info.change_pass) {
      if (
        this.user_info.firstname != '' &&
        this.user_info.lastname != '' &&
        this.user_info.email != '' &&
        this.user_info.gender != ''
      ) {
        this.presentPrompt(this.user_info);
      } else {
        let alert = this.alert.create({
          title: 'Error',
          subTitle: 'fill out all necessary fields',
          buttons: ['Continue']
        });
        alert.present();
      }
    } else {
      if (
        this.user_info.firstname != '' &&
        this.user_info.lastname != '' &&
        this.user_info.email != '' &&
        this.user_info.gender != '' &&
        this.user_info.password != '' &&
        this.user_info.new_password != '' &&
        this.user_info.conf_password != '' &&
        this.user_info.new_password == this.user_info.conf_password
      ) {
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        this.pushkartService.changeUserDetails(this.user_info).subscribe(
          data => {
            this.presentAlert(data);
            loading.dismiss();
            this.user_info.change_pass = false;
          }, err => {
            this.presentAlert('fail')
            console.log(err);
            loading.dismiss();
          }
        )

      } else {
        let alert = this.alert.create({
          title: 'Error',
          subTitle: 'fill out all necessary fields',
          buttons: ['Continue']
        });
        alert.present();
      }
    }

  }

  presentPrompt(user) {
    let alert = this.alert.create({
      title: 'Login',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Login',
          handler: data => {
            user.password = data.password;
            user.new_password = data.password;
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            this.pushkartService.changeUserDetails(user).subscribe(
              data => {
                loading.dismiss();
                this.presentAlert(data);
                return true;
              }, err => {
                loading.dismiss();
                console.log(err);
                return false;
              }
            )

          }
        }
      ]
    });
    alert.present();
  }

  presentAlert(data) {
    var title = 'password is incorrect';
    var message = 'enter valid password';
    if (data == 'Success') {
      title = 'Success';
      message = 'User Information Changed'
    }
    let alert = this.alert.create({
      title: title,
      subTitle: message,
      buttons: ['Continue']
    });
    alert.present();
  }

  addNewAddress() {
    let modal = this.modal.create(ModalAddressPage, { 'edit': false });
    modal.present();
    modal.onDidDismiss(
      data => {
        if (data) {
          this.getAddresses();
        }
      }
    )
  }

  editAddress(index) {
    console.log('index: ' + (index));
    console.log('editing: %o', this.group_address[index]);
    let modal = this.modal.create(
      ModalAddressPage,
      {
        'edit': true,
        'address': this.group_address[index]
      }
    );

    modal.present();
    modal.onDidDismiss(
      data => {
        if (data) {
          this.getAddresses();
        }
      }
    )
  }

  removeAddress(index) {
    console.log('index: ' + (index));
    console.log('removing: %o', this.group_address[index]);

    let id = this.group_address[index].entity_id;
    this.userService.deleteAddress(id).subscribe(
      data => {
        let alert = this.alert.create({
          title: 'Success',
          subTitle: 'address deleted',
          buttons: ['Continue']
        });
        alert.present();
        this.getAddresses();
      }, err => {
        let alert = this.alert.create({
          title: 'Failed',
          subTitle: 'something happened',
          buttons: ['Dismiss']
        });
        alert.present();
      }
    )
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    var offset = 0;
    this.myTab1.nativeElement.className = 'item';
    this.myTab2.nativeElement.className = 'item';
    this.myTab3.nativeElement.className = 'item';
    this.myTab4.nativeElement.className = 'item';
    this.myTab5.nativeElement.className = 'item';
    this.myTab6.nativeElement.className = 'item';


    switch (currentIndex) {
      case 0: {
        offset = this.myTab1.nativeElement.offsetLeft;
        this.myTab1.nativeElement.className = 'item active-block';
        break;
      }
      case 1: {
        offset = this.myTab2.nativeElement.offsetLeft;
        this.myTab2.nativeElement.className = 'item active-block';
        break;
      }
      case 2: {
        offset = this.myTab3.nativeElement.offsetLeft;
        this.myTab3.nativeElement.className = 'item active-block';
        break;
      }
      case 3: {
        offset = this.myTab4.nativeElement.offsetLeft;
        this.myTab4.nativeElement.className = 'item active-block';
        break;
      }
      case 4: {
        offset = this.myTab5.nativeElement.offsetLeft;
        this.myTab5.nativeElement.className = 'item active-block';
        break;
      }
      default: {
        offset = this.myTab6.nativeElement.offsetLeft;
        this.myTab6.nativeElement.className = 'item active-block';
        break;
      }
    }
    this.scrollTo(offset);
  }
  scrollTo(xoffset) {
    this.myTablist.nativeElement.scrollLeft = xoffset;
  }

  setPage(index) {

    this.slides.slideTo(index, 500);
    /* setTimeout(() => {
      this.slides.lockSwipes(true);
    }, 500); */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaccountPage');
    console.log('order-list'+this.dashboard_order_list);
    this.slides.lockSwipes(false);
    //this.slides.lockSwipes(true);
    this.scrollgrid.ionScrollStart.subscribe(data => {
      console.log('scrollstart');
      this.slides.lockSwipes(true);
    });
    this.scrollgrid.ionScrollEnd.subscribe(data => {
      this.slides.lockSwipes(false);
      console.log('scrollend');
    })
    if (this.start_page != 0) {
      setTimeout(() => {
        this.setPage(this.start_page);
      }, 1000);
    }


  }
  disablescroll() {
    this.slides.lockSwipes(true);
  }
  /* openMenu() {
    this.menu.open('category');
    this.menu.toggle();
  }
 */
} 
