import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NeedHelpPage } from './need-help';

@NgModule({
  declarations: [
    NeedHelpPage,
  ],
  imports: [
    IonicPageModule.forChild(NeedHelpPage),
  ],
  exports: [
    NeedHelpPage
  ]
})
export class NeedHelpPageModule {}
